<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusFaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_fares', function (Blueprint $table) {
            $table->increments('busfare_id');
            $table->string('busfare_deviceid');
            $table->string('busfare_plateno');
            $table->string('busfare_totalprice');
            $table->string('busfare_tripname');
            $table->time('busfare_arrive');
            $table->time('busfare_depart');
            $table->date('busfare_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_fares');
    }
}
