<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreteBusPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   
    public function up()
    {
        Schema::create('bus_prices', function (Blueprint $table) {
            $table->increments('busprice_id');
            $table->integer('people_count');
            $table->integer('busStop');
            $table->integer('busstop_price');
            $table->time('busstop_time');
            $table->integer('busfareid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_prices');
    }
}
