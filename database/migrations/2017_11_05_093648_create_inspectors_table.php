<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspectors', function (Blueprint $table) {
            $table->increments('inspector_id');
            $table->string('inspector_rfid');
            $table->string('inspector_name');
            $table->string('inspector_nrc');
            $table->string('inspector_dob');
            $table->string('inspector_company');
            $table->text('inspector_address');
            $table->string('inspector_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspectors');
    }
}
