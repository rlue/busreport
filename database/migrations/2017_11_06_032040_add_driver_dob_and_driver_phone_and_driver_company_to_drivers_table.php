<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverDobAndDriverPhoneAndDriverCompanyToDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            //
             $table->date("driver_dob");
             $table->string("driver_phone");
             $table->integer("driver_company");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function (Blueprint $table) {
            //
            $table->dropColumn('driver_dob');
            $table->dropColumn('driver_phone');
            $table->dropColumn('driver_company');
        });
    }
}
