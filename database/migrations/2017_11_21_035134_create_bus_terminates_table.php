<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusTerminatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_terminates', function (Blueprint $table) {
            $table->increments('terminate_id');
            $table->integer('terminate_plateno');
            $table->integer('terminate_deviceid');
            $table->integer('terminate_tripname');
            $table->text('terminate_reason');
            $table->date('terminate_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_terminates');
    }
}
