<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_registers', function (Blueprint $table) {
            $table->increments('busregister_id');
            $table->string('bus_plateno');
            $table->integer('busName');
            $table->integer('busCompany');
            $table->string('bus_manufacturer');
            $table->string('bus_model');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_registers');
    }
}
