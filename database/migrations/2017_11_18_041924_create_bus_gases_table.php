<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusGasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_gases', function (Blueprint $table) {
            $table->increments('gas_id');
            $table->integer('gas_plateno');
            $table->string('gas_deviceid');
            $table->integer('gas_tripname');
            $table->integer('gas_unit');
            $table->integer('gas_distance');
            $table->date('gas_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_gases');
    }
}
