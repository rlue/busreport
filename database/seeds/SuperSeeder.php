<?php

use Illuminate\Database\Seeder;
use App\User;
class SuperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         User::create([
            'name'  => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password'  => bcrypt("minminoo123"),
            'is_super'  => true
            ]);
    }
}
