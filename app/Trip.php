<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bus;
class Trip extends Model
{
    //
     protected $fillable = [
        'route_name', 'trip_description', 'trip_fare','trip_group','trip_bus'
    ];
    protected $primaryKey = 'trip_id';

     public function bus()
    {
        return $this->belongsTo(Bus::class,'trip_bus');
    }
}
