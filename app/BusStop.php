<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Street;
use App\Township;
class BusStop extends Model
{
    //
    protected $table = 'bus_stops';

     protected $fillable = [
        'busstop_name','busstop_code','busstop_street','busstop_township','busstop_lat','busstop_long',
    ];

   public function street()
    {
        return $this->belongsTo(Street::class,'busstop_street');
    }

    public function township()
    {
        return $this->belongsTo(Township::class,'busstop_township');
    }

}
