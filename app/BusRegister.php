<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bus;
use App\BusCompany;

class BusRegister extends Model
{
    //
     protected $fillable = [
        'bus_plateno','busName','busCompany','bus_manufacturer','bus_model'
    ];
    protected $primaryKey = 'busregister_id';


    public function bus()
    {
        return $this->belongsTo(Bus::class,'busName');
    }

   
}
