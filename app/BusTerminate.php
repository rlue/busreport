<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusTerminate extends Model
{
    //

     protected $fillable = [
        'terminate_plateno','terminate_deviceid','terminate_tripname','terminate_reason','terminate_date'
    ];

    protected $primaryKey = 'terminate_id';
}
