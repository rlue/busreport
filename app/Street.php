<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BusStop;
class Street extends Model
{
    //
    protected $fillable = [
        'street_name'
    ];
    
}
