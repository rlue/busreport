<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bus;
use App\BusCompany;
class Driver extends Model
{
    //
    protected $fillable = [
        'driver_rfid','driver_captainName','driver_nrcno','drivinglicence','driver_address','driver_blacklist','bl_refno','driver_dob','driver_phone','driver_company'
    ];
    protected $primaryKey = 'driver_id';

     public function Bus()
    {
        return $this->belongsTo(Bus::class,'bl_refno');
    }
    public function Company()
    {
        return $this->belongsTo(BusCompany::class,'driver_company');
    }
}
