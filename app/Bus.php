<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    //
     protected $fillable = [
        'bus_name'
    ];
    protected $primaryKey = 'bus_id';
}
