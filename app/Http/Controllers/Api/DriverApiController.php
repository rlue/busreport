<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Driver;
use App\BusCompany;
class DriverApiController extends Controller
{
    //

    public function index(Request $request)
    {
    	$rfid = $request->input('rfid_id');

    	$driver = Driver::with('company')->where('driver_rfid','=',$rfid)->get();
    	if(!$driver->isEmpty()){
             $data = [
            'data' => 1,
            'result' => $driver
        ];
    		return response()->json($data);
    	}else{
             $data = [
            'data' => 0,
            'result' => $driver
        ];
    		return response()->json($data);
    	}
    	
    }
}
