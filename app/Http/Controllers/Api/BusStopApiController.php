<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusStop;
class BusStopApiController extends Controller
{
    //


    public function index()
    {
    	$busstop=[];
    	$busstop = BusStop::with(['street','township'])->get();

    	if(!$busstop->isEmpty()){
             $data = [
            'data' => 1,
            'result' => $busstop
        ];
    		return response()->json($data);
    	}else{
             $data = [
            'data' => 0,
            'result' => $busstop
        ];
    		return response()->json($data);
    	}
    }
}
