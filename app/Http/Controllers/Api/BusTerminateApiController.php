<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusTerminate;
use Validator;
use Carbon\Carbon;
class BusTerminateApiController extends Controller
{
    //

    public function index(Request $request)
    {

         $validator = Validator::make($request->all(), [
            'terminate_plateno' => 'integer',
            'terminate_deviceid' => 'integer',
            'terminate_tripname' => 'integer',
            
        ]);

        if ($validator->fails()) {
             $message = [
            'result'=>'Not Success',
            'data' => 0
            ];
            return response()->json($message);

        }else{

        }
    	 $data = [
        
        'terminate_plateno'      => $request->input("terminate_plateno"),
        'terminate_deviceid'      => $request->input("terminate_deviceid"),
        'terminate_tripname'      => $request->input("terminate_tripname"),
        'terminate_reason'      => $request->input("terminate_reason"),
        'terminate_date'      => Carbon::now(),
       
        ];
        //return response()->json($data, 201);
        $id = BusTerminate::create($data);

        
            $message = [
            'result'=> 'Successful',
            'data' => 1
            ];
        	return response()->json($message);
       
    }
}
