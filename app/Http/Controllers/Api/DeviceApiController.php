<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Device;
use DB;
class DeviceApiController extends Controller
{
    //
    public function index(Request $request)
    {
    	$deviceid = $request->input('device_id');

    	$deviceall = DB::table('devices')
            ->leftJoin('bus_registers', 'devices.plateNO', '=', 'bus_registers.busregister_id')
            ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
            ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
            //->leftJoin('trips', 'devices.plateNO', '=', 'trips.trip_bus')
            ->where('deviceID','=',$deviceid)
            ->get();
            $data = [];
         if(!$deviceall->isEmpty()){
          
        
            foreach($deviceall as $d){
            	$busregister= [
                    'busregister_id' => $d->busregister_id,
            		'bus_plateno' => $d->bus_plateno,
            		'busName' => $d->bus_name,
            		'busCompany' => $d->busCompany,
            		'bus_manufacturer' => $d->bus_manufacturer,
            		'bus_model' => $d->bus_model,

            	];
            $trips = DB::table('trips')->where('trip_bus','=',$d->bus_id)->get();
            if(!$trips->isEmpty()){
                foreach($trips as $t){
                $trip[] = [
                    'trip_id' => $t->trip_id,
                    'route_name' => $t->route_name,
                    'trip_description' =>$t->trip_description,
                    'trip_fare' => $t->trip_fare,
                    'trip_group'   => $t->trip_group,
                    'trip_bus' => $t->trip_bus,
                    

                ];
            }
            }
            
            	
            	$company = [
            		'company_id' => $d->company_id,
            		'company_name' => $d->company_name,
            		'contact_person' => $d->contact_person,
            		'company_phone' => $d->company_phone,
            		'company_mobile' => $d->company_mobile,

            	];
            	$data = [
                    'deivceid_id' => $d->device_id,
            		'device_id' => $d->deviceID,
            		'bus_register' => $busregister,
            		'trip' => $trip,
            		'company' => $company,
            	];
            }
    	
             $message = [
              'data' => 1,
            'result'=> $data
           
            ];
        	return response()->json($message);
        }else{
             $message = [
             'data' => 1,
            'result'=> $data
            
            ];
        	return response()->json($message);
        }
    }
}
