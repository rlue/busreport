<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusFare;
use App\BusPrice;
use Validator;
use Carbon\Carbon;
class BusApiFareController extends Controller
{
    //

	public function index()
	{
		$busstop = BusStop::get();
		return response()->json($busstop, 201);
	}
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'busfare_deviceid' => 'integer',
            'busfare_plateno' => 'integer',
            'busfare_tripname' => 'integer',
           
        ]);
        
               
       
        if ($validator->fails()) {
             $datas = [
                    'data' => 0,
                    'result' => 'Not Success'
                ];
                    return response()->json($datas);

        }else{
             $input = Input::all();
       
        $data = [
        'busfare_deviceid'      => $request->input("busfare_deviceid"),
        'busfare_plateno'      => $request->input("busfare_plateno"),
        'busfare_totalprice'      => $request->input("busfare_totalprice"),
        'busfare_tripname'      => $request->input("busfare_tripname"),
        'busfare_arrive'      => $request->input("busfare_arrive"),
        'busfare_depart'      => $request->input("busfare_depart"),
        'busfare_date'      => Carbon::now(),
       
        ];
        //return response()->json($data, 201);
        $id = BusFare::create($data)->busfare_id;

            $condition =     json_decode( $input['people_count'] , true);
            $busStop =     json_decode( $input['busStop'] , true);
            $busstop_price =     json_decode( $input['busstop_price'] , true);
            
            foreach ($condition as $key => $cond) {
                 $student = new BusPrice;
                 $student->people_count = $condition[$key];
                 $student->busStop = $busStop[$key];
                 $student->busstop_price = $busstop_price[$key];
                 $student->busstop_time = $input['busstop_time'];
                 $student->busfareid = $id;
                 $student->save();
             }
                //$busfare =  BusFare::create($request->all());
                
                $datas = [
                    'data' => 1,
                    'result' => 'Successful'
                ];
                return response()->json($datas);
               
        }
       
    }
}
