<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Inspector;
use App\BusCompany;
class InspectorApiController extends Controller
{
    //
     public function index(Request $request)
    {
    	$rfid = $request->input('inspector_rfid');

    	$inspector = Inspector::with('company')->where('inspector_rfid','=',$rfid)->get();

    	if(!$inspector->isEmpty()){
             $data = [
            'data' => 1,
            'result' => $inspector
        ];
    		return response()->json($data);
    	}else{
             $data = [
            'data' => 0,
            'result' => $inspector
        ];
    		return response()->json($data);
    	}
    	
    }
}
