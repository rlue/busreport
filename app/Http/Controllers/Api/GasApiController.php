<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\BusGas;
use Carbon\Carbon;
class GasApiController extends Controller
{
    //
    public function index()
	{
		$busgas = BusGas::get();
		return response()->json($busgas, 201);
	}
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'gas_plateno' => 'integer',
            'gas_deviceid' => 'integer',
            'gas_tripname' => 'integer',
            'gas_unit' => 'integer',
            'gas_distance' => 'integer',
        ]);

        if ($validator->fails()) {
            $message = [
            'result'=>'Not Success',
            'data'=> 0
            ];
            return response()->json($message);
        }else{
             $data = [
        
            'gas_plateno'      => $request->input("gas_plateno"),
            'gas_deviceid'      => $request->input("gas_deviceid"),
            'gas_tripname'      => $request->input("gas_tripname"),
            'gas_unit'      => $request->input("gas_unit"),
            'gas_distance'  =>$request->input('gas_distance'),
            'gas_date'      => Carbon::now(),
            ];
            $busgas =  BusGas::create($data);

            
            $message = [
                'result'=>'Successful',
                'data' => 1
                ];
            return response()->json($message);
       
        
        }	
    	
    }
}
