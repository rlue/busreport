<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusFare;
use App\Trip;
use App\BusStop;
use App\BusCompany;
use App\Bus;
use DB;
use Excel;
use Session;
class BusFareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $busfare = DB::table('bus_fares')
        ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
         ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
        ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
        ->get();
        $company = BusCompany::get();
        return view("admin.busfare.index", compact("busfare","company"));
    }

    public function byBusExport(Request $request)
    {
        $getCompany = $request->input('getCompany');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');
        $filetype = $request->input('file_type');

        if(!empty($getCompany) || !empty($startDate) && !empty($endDate)){

            if(!empty($getCompany) && !empty($startDate) && !empty($endDate)){

                $busfare = DB::table('bus_fares')
                ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                ->where('company_id','=',$getCompany)
                ->where('busfare_date','>=',$startDate)
                ->where('busfare_date','<=',$endDate)
                ->get();
                if(!$busfare->isEmpty()){
                 

                    foreach ($busfare as $stop) {
                            $blateno[] = $stop->busregister_id;
                            $company = $stop->company_name;
                    }
                
                    $blate_price =array_unique($blateno);
                    $num = count($blate_price);
                    $currentdate = Date('d-M-Y');
                    $busStopArr[0] = ['Detail Trip Report By Bus', '','','',''];
                    $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                    $busStopArr[] = ['Start Date : '.$startDate,'', '','End Date :'.$endDate];
                    $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                    $busStopArr[] = ['', '', '','',''];
                    $busStopArr[] = ['No', 'License Plate No.', 'Start Date','End Date','Total'];
                    $count = 1;
                    $alltotal = 0;
                    foreach ($blate_price as $key => $value) {
                       $busprice = DB::table('bus_fares')
                        ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                        ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                        ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                        ->where('busregister_id','=',$value)
                        ->where('busfare_date','>=',$startDate)
                        ->where('busfare_date','<=',$endDate)
                        ->get(); 
                         
                        
                        $total = 0;
                        foreach ($busprice as $stop) {
                        $total += $stop->busfare_totalprice;
                        $name = $stop->bus_plateno;
                        $create = $stop->created_at;
                        }

                        $alltotal += $total;
                        $busStopArr[] = [$count++, $name, $startDate , $endDate ,$total];

                    }  
                    
                        $busStopArr[] = ['','','','Total (Kyats)',$alltotal]; 
                   
                    Excel::create('ByBus', function($excel) use($busStopArr) {
                      $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                          $sheet->setFontFamily('Zawgyi-One');
                          $sheet->setAutoSize(true);
                          $sheet->mergeCells('A1:B1');
                          $sheet->mergeCells('A2:B2');
                          $sheet->mergeCells('A3:B3');
                          $sheet->mergeCells('A4:B4');
                          $sheet->mergeCells('D2:E2');
                          $sheet->mergeCells('D3:E3');
                          $sheet->setHeight(1, 30);
                          $sheet->setHeight(2, 20);
                          $sheet->setHeight(3, 20);
                          $sheet->setHeight(4, 20);
                          $sheet->setHeight(5, 30);
                          $sheet->setHeight(6, 25);
                          $sheet->setHeight(15);
                          $sheet->setAutoSize(true);
                          $sheet->setTitle('Detail Trip Report By Bus');
                        
                        $sheet->fromArray($busStopArr, null, 'A1', false, false);
                        $sheet->row(1, function ($row) {
                            $row->setFontWeight('bold');
                            $row->setFontSize(14);
                           

                        });
                        $sheet->row(6, function ($row) {
                            $row->setFontWeight('bold');
                            $row->setBackground('#ebebeb');

                        });
                                               
                        
                      });
                    })->export($filetype);

                }else{
                    $company = BusCompany::get();
                    Session::flash('message', 'Sorry You export Empty Data');
                   
                    return view('admin.busfare.index',compact('company'));

                }  
            }else if(!empty($getCompany) && empty($startDate) && empty($endDate)){
                $busfare = DB::table('bus_fares')
                ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                ->where('company_id','=',$getCompany)
                ->get();
                if(!$busfare->isEmpty()){
                 

                    foreach ($busfare as $stop) {
                            $blateno[] = $stop->busregister_id;
                            $company = $stop->company_name;
                    }
                    
                    $blate_price =array_unique($blateno);
                    $num = count($blate_price);
                    $currentdate = Date('d-M-Y');
                    $busStopArr[0] = ['Detail Trip Report By Bus', '','','',''];
                    $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                    $busStopArr[] = ['Start Date : '.$startDate,'', '','End Date :'.$endDate];
                    $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                     $busStopArr[] = ['', '', '','',''];
                    $busStopArr[] = ['No', 'License Plate No.', 'Start Date','End Date','Total'];
                    $count = 1;
                    $alltotal = 0;
                    foreach ($blate_price as $key => $value) {
                       $busprice = DB::table('bus_fares')
                        ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                        ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                        ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                        ->where('busregister_id','=',$value)
                        ->get(); 
                         
                        
                        $total = 0;
                        foreach ($busprice as $stop) {
                        $total += $stop->busfare_totalprice;
                        $name = $stop->bus_plateno;
                        $create = $stop->created_at;
                        }
                        $alltotal += $total;
                         $busStopArr[] = [$count++, $name, $startDate , $endDate ,$total];
                    }   
             
                     $busStopArr[] = ['','','','Total (Kyats)',$alltotal]; 
                   
                    Excel::create('ByBus', function($excel) use($busStopArr) {
                      $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                          $sheet->setFontFamily('Zawgyi-One');
                          $sheet->mergeCells('A1:B1:C1');
                          $sheet->mergeCells('A2:B2');
                          $sheet->mergeCells('A3:B3');
                          $sheet->mergeCells('A4:B4');
                          $sheet->mergeCells('D2:E2');
                          $sheet->mergeCells('D3:E3');
                          $sheet->setHeight(1, 30);
                          $sheet->setHeight(2, 20);
                          $sheet->setHeight(3, 20);
                          $sheet->setHeight(4, 20);
                          $sheet->setHeight(5, 30);
                          $sheet->setHeight(6, 25);
                          $sheet->setHeight(15);
                          $sheet->setAutoSize(true);
                          $sheet->setTitle('Detail Trip Report By Bus');
                        
                        $sheet->fromArray($busStopArr, null, 'A1', false, false);
                        $sheet->row(1, function ($row) {
                            $row->setFontWeight('bold');
                            $row->setFontSize(14);
                           

                        });
                        $sheet->row(6, function ($row) {
                            $row->setFontWeight('bold');
                            $row->setBackground('#ebebeb');

                        });
                                               
                        
                      });
                    })->export($filetype);

                }else{
                     $company = BusCompany::get();
                    Session::flash('message', 'Sorry You export Empty Data');
                   
                    return view('admin.busfare.index',compact('company'));

                }  
            }else{
                $busfare = DB::table('bus_fares')
                ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                //->where('company_id','=',$getCompany)
                ->where('busfare_date','>=',$startDate)
                ->where('busfare_date','<=',$endDate)
                ->get();
                if(!$busfare->isEmpty()){
                 

                    foreach ($busfare as $stop) {
                            $blateno[] = $stop->busregister_id;
                            $company = '';
                    }
                
                    $blate_price =array_unique($blateno);
                    $num = count($blate_price);
                    $currentdate = Date('d-M-Y');
                    $busStopArr[0] = ['Detail Trip Report By Bus', '','','',''];
                    $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                    $busStopArr[] = ['Start Date : '.$startDate,'', '','End Date :'.$endDate];
                    $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                     $busStopArr[] = ['', '', '','',''];
                    $busStopArr[] = ['No', 'License Plate No.', 'Start Date','End Date','Total'];
                     $count = 1;
                     $alltotal = 0;
                    foreach ($blate_price as $key => $value) {
                       $busprice = DB::table('bus_fares')
                        ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                        ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                        ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                        ->where('busregister_id','=',$value)
                        ->where('busfare_date','>=',$startDate)
                        ->where('busfare_date','<=',$endDate)
                        ->get(); 
                         
                        
                        $total = 0;
                        foreach ($busprice as $stop) {
                        $total += $stop->busfare_totalprice;
                        $name = $stop->bus_plateno;
                        $create = $stop->created_at;
                        }
                        $alltotal += $total;
                        $busStopArr[] = [$count++, $name, $startDate , $endDate ,$total];
                    }   
             
                    $busStopArr[] = ['','','','Total (Kyats)',$alltotal]; 
                   
                    Excel::create('ByBus', function($excel) use($busStopArr) {
                      $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                          $sheet->setFontFamily('Zawgyi-One');
                           $sheet->mergeCells('A1:B1:C1');
                          $sheet->mergeCells('A2:B2');
                          $sheet->mergeCells('A3:B3');
                          $sheet->mergeCells('A4:B4');
                          $sheet->mergeCells('D2:E2');
                          $sheet->mergeCells('D3:E3');
                          $sheet->setHeight(1, 30);
                          $sheet->setHeight(2, 20);
                          $sheet->setHeight(3, 20);
                          $sheet->setHeight(4, 20);
                          $sheet->setHeight(5, 30);
                          $sheet->setHeight(6, 25);
                          $sheet->setHeight(15);
                          $sheet->setAutoSize(true);
                          $sheet->setTitle('Detail Trip Report By Bus');
                        
                        $sheet->fromArray($busStopArr, null, 'A1', false, false);
                        $sheet->row(1, function ($row) {
                            $row->setFontWeight('bold');
                            $row->setFontSize(14);
                           

                        });
                        $sheet->row(6, function ($row) {
                            $row->setFontWeight('bold');
                            $row->setBackground('#ebebeb');

                        });
                                               
                        
                      });
                    })->export($filetype);

                }else{
                     $company = BusCompany::get();
                    Session::flash('message', 'Sorry You export Empty Data');
                   
                    return view('admin.busfare.index',compact('company'));

                }  
            }
           
        }else{
                $company = BusCompany::get();
                Session::flash('message', 'Please Select company and require Start Date and end Date');
               
                return view('admin.busfare.index',compact('company'));
        }
      
       
         

     
         
    }

    public function ByRoute()
    {
        $company = BusCompany::get();
        return view("admin.busfare.byRoute", compact("company"));
    }

    public function ByRouteExport(Request $request){

        $getCompany = $request->input('getCompany');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');
        $filetype = $request->input('file_type');
        if(!empty($getCompany) || !empty($startDate) && !empty($endDate)){

            if(!empty($getCompany) && !empty($startDate) && !empty($endDate)){
                $busfare = DB::table('bus_fares')
                    ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                    ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                    ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                    ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
                    ->where('company_id','=',$getCompany)
                    ->where('busfare_date','>=',$startDate)
                    ->where('busfare_date','<=',$endDate)
                    ->get();

                    if(!$busfare->isEmpty()){
                     

                        foreach ($busfare as $stop) {
                                $busID[] = $stop->bus_id;
                                $company = $stop->company_name;
                        }
                       
                        $blate_price =array_unique($busID);
                        $num = count($blate_price);
                        $currentdate = Date('d-M-Y');
                        $busStopArr[0] = ['Gas Refill Report By Bus', 'asdfsdfsd','','',''];
                        $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                        $busStopArr[] = ['Start Date : '.$startDate,'', 'aa','End Date :'.$endDate];
                        $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                        $busStopArr[] = ['', '', '','',''];
                        $busStopArr[] = ['No', 'Route Name', 'Start Date','End Date','Total'];
                        $count = 1;
                        $alltotal = 0;
                        foreach ($blate_price as $key => $value) {
                           $busprice = DB::table('bus_fares')
                            ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                            ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                            ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                            ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
                            ->where('bus_id','=',$value)
                            ->where('busfare_date','>=',$startDate)
                            ->where('busfare_date','<=',$endDate)
                            ->get(); 
                             
                            
                            $total = 0;
                            foreach ($busprice as $stop) {
                            $total += $stop->busfare_totalprice;
                            $name = $stop->bus_name;
                            $create = $stop->created_at;
                            }

                            $alltotal += $total;
                            $busStopArr[] = [$count++, $name, $startDate , $endDate ,$total];

                        }  
                        
                            $busStopArr[] = ['','','','Total (Kyats)',$alltotal]; 
                       
                        Excel::create('ByRoute', function($excel) use($busStopArr) {
                          $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                              $sheet->setFontFamily('Zawgyi-One');
                              $sheet->mergeCells('A1:B1:C1');
                              $sheet->mergeCells('A2:B2');
                              $sheet->mergeCells('A3:B3');
                              $sheet->mergeCells('A4:B4');
                              $sheet->mergeCells('D2:E2');
                              $sheet->mergeCells('D3:E3');
                              $sheet->setHeight(1, 30);
                              $sheet->setHeight(2, 20);
                              $sheet->setHeight(3, 20);
                              $sheet->setHeight(4, 20);
                              $sheet->setHeight(5, 30);
                              $sheet->setHeight(6, 25);
                              $sheet->setHeight(15);
                              $sheet->setAutoSize(true);
                              $sheet->setTitle('Detail Trip Report By Bus');
                            
                            $sheet->fromArray($busStopArr, null, 'A1', false, false);
                            $sheet->row(1, function ($row) {
                                $row->setFontWeight('bold');
                                $row->setFontSize(14);
                               

                            });
                            $sheet->row(6, function ($row) {
                                $row->setFontWeight('bold');
                                $row->setBackground('#ebebeb');

                            });
                                                   
                            
                          });
                        })->export($filetype);

                    }else{
                        $company = BusCompany::get();
                        Session::flash('message', 'Sorry You export Empty Data');
                       
                        return view('admin.busfare.byRoute',compact('company'));

                    }  
            }else if(!empty($getCompany) && empty($startDate) && empty($endDate)){

                  $busfare = DB::table('bus_fares')
                    ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                    ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                    ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                    ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
                    ->where('company_id','=',$getCompany)
                    ->get();

                    if(!$busfare->isEmpty()){
                     

                        foreach ($busfare as $stop) {
                                $busID[] = $stop->bus_id;
                                $company = $stop->company_name;
                        }
                       
                        $blate_price =array_unique($busID);
                        $num = count($blate_price);
                        $currentdate = Date('d-M-Y');
                        $busStopArr[0] = ['Detail Trip Report By Route', '','','',''];
                        $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                        $busStopArr[] = ['Start Date : '.$startDate,'', '','End Date :'.$endDate];
                        $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                        $busStopArr[] = ['', '', '','',''];
                        $busStopArr[] = ['No', 'Route Name', 'Start Date','End Date','Total'];
                        $count = 1;
                        $alltotal = 0;
                        foreach ($blate_price as $key => $value) {
                           $busprice = DB::table('bus_fares')
                            ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                            ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                            ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                            ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
                            ->where('bus_id','=',$value)
                            ->get(); 
                             
                            
                            $total = 0;
                            foreach ($busprice as $stop) {
                            $total += $stop->busfare_totalprice;
                            $name = $stop->bus_name;
                            $create = $stop->created_at;
                            }

                            $alltotal += $total;
                            $busStopArr[] = [$count++, $name, $startDate , $endDate ,$total];

                        }  
                        
                            $busStopArr[] = ['','','','Total (Kyats)',$alltotal]; 
                       
                        Excel::create('ByRoute', function($excel) use($busStopArr) {
                          $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                              $sheet->setFontFamily('Zawgyi-One');
                              $sheet->mergeCells('A1:B1:C1');
                              $sheet->mergeCells('A2:B2');
                              $sheet->mergeCells('A3:B3');
                              $sheet->mergeCells('A4:B4');
                              $sheet->mergeCells('D2:E2');
                              $sheet->mergeCells('D3:E3');
                              $sheet->setHeight(1, 30);
                              $sheet->setHeight(2, 20);
                              $sheet->setHeight(3, 20);
                              $sheet->setHeight(4, 20);
                              $sheet->setHeight(5, 30);
                              $sheet->setHeight(6, 25);
                              $sheet->setHeight(15);
                              $sheet->setAutoSize(true);
                              $sheet->setTitle('Detail Trip Report By Bus');
                            
                            $sheet->fromArray($busStopArr, null, 'A1', false, false);
                            $sheet->row(1, function ($row) {
                                $row->setFontWeight('bold');
                                $row->setFontSize(14);
                               

                            });
                            $sheet->row(6, function ($row) {
                                $row->setFontWeight('bold');
                                $row->setBackground('#ebebeb');

                            });
                                                   
                            
                          });
                        })->export($filetype);

                    }else{
                        $company = BusCompany::get();
                        Session::flash('message', 'Sorry You export Empty Data');
                       
                        return view('admin.busfare.byRoute',compact('company'));

                    }  
            }else{
                  $busfare = DB::table('bus_fares')
                    ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                    ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                    ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                    ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
                    ->where('busfare_date','>=',$startDate)
                    ->where('busfare_date','<=',$endDate)
                    ->get();

                    if(!$busfare->isEmpty()){
                     

                        foreach ($busfare as $stop) {
                                $busID[] = $stop->bus_id;
                                $company = '';
                        }
                       
                        $blate_price =array_unique($busID);
                        $num = count($blate_price);
                        $currentdate = Date('d-M-Y');
                        $busStopArr[0] = ['Detail Trip Report By Route', '','','',''];
                        $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                        $busStopArr[] = ['Start Date : '.$startDate,'', '','End Date :'.$endDate];
                        $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                        $busStopArr[] = ['', '', '','',''];
                        $busStopArr[] = ['No', 'Route Name', 'Start Date','End Date','Total'];
                        $count = 1;
                        $alltotal = 0;
                        foreach ($blate_price as $key => $value) {
                           $busprice = DB::table('bus_fares')
                            ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                            ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                            ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                            ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
                            ->where('bus_id','=',$value)
                            ->where('busfare_date','>=',$startDate)
                            ->where('busfare_date','<=',$endDate)
                            ->get(); 
                             
                            
                            $total = 0;
                            foreach ($busprice as $stop) {
                            $total += $stop->busfare_totalprice;
                            $name = $stop->bus_name;
                            $create = $stop->created_at;
                            }

                            $alltotal += $total;
                            $busStopArr[] = [$count++, $name, $startDate , $endDate ,$total];

                        }  
                        
                            $busStopArr[] = ['','','','Total (Kyats)',$alltotal]; 
                       
                        Excel::create('ByRoute', function($excel) use($busStopArr) {
                          $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                              $sheet->setFontFamily('Zawgyi-One');
                              $sheet->mergeCells('A1:B1:C1');
                              $sheet->mergeCells('A2:B2');
                              $sheet->mergeCells('A3:B3');
                              $sheet->mergeCells('A4:B4');
                              $sheet->mergeCells('D2:E2');
                              $sheet->mergeCells('D3:E3');
                              $sheet->setHeight(1, 30);
                              $sheet->setHeight(2, 20);
                              $sheet->setHeight(3, 20);
                              $sheet->setHeight(4, 20);
                              $sheet->setHeight(5, 30);
                              $sheet->setHeight(6, 25);
                              $sheet->setHeight(15);
                              $sheet->setAutoSize(true);
                              $sheet->setTitle('Detail Trip Report By Bus');
                            
                            $sheet->fromArray($busStopArr, null, 'A1', false, false);
                            $sheet->row(1, function ($row) {
                                $row->setFontWeight('bold');
                                $row->setFontSize(14);
                               

                            });
                            $sheet->row(6, function ($row) {
                                $row->setFontWeight('bold');
                                $row->setBackground('#ebebeb');

                            });
                                                   
                            
                          });
                        })->export($filetype);

                    }else{
                        $company = BusCompany::get();
                        Session::flash('message', 'Sorry You export Empty Data');
                       
                        return view('admin.busfare.byRoute',compact('company'));

                    }  
            }
              
           
        }else{
                $company = BusCompany::get();
                Session::flash('message', 'Please Select company OR require Start Date and end Date');
               
                return view('admin.busfare.byRoute',compact('company'));
        }
    }

    public function ByTrip()
    {
      $bus = Bus::get();
      $company = BusCompany::get();
        return view("admin.busfare.byTrip", compact("company","bus"));
    }
    public function ByTripExport(Request $request){
        $getCompany = $request->input('getCompany');
        $startDate = $request->input('startDate');
       
        $filetype = $request->input('file_type');
        $bus = $request->input('getBus');

        if(!empty($startDate) && !empty($bus)){
          if(!empty($getCompany)){
               $busfare = DB::table('bus_fares')
                    ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                    ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
                    ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                    ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                     ->where('busCompany','=',$getCompany)
                    ->where('busfare_date',$startDate)
                    ->where('busName','=',$bus)
                    ->get();
          }else{
             $busfare = DB::table('bus_fares')
                    ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                    ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
                    ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                    ->leftJoin('trips', 'bus_fares.busfare_tripname', '=', 'trips.trip_id')
                    ->where('busfare_date',$startDate)
                    ->where('busName','=',$bus)
                    ->get();
          }
        
            
           
                    if(!$busfare->isEmpty()){
                     

                        foreach ($busfare as $stop) {

                         

                                $company = $stop->company_name;
                                $blateno[] = $stop->busregister_id;
                                $trip[] = $stop->busfare_tripname;
                                $buss[] = $stop->bus_name;

                                 
                        }
                      
                        $busplate =array_unique($blateno);

                         
                             
                        $num = count($busplate);
                        $currentdate = Date('d-M-Y');
                        $busStopArr[0] = ['Detail Trip Report By Route', '','','',''];
                        $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                        $busStopArr[] = ['Start Date : '.$startDate,'', '',''];
                        $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                        $busStopArr[] = ['', '', '','',''];

                        $triptitle[] = 'Sr';
                        $triptitle[] = 'Plate No';
                       
                        for($i=0;$i< 4;$i++){
                         $triptitle[] = '';
                         $triptitle[] = '';
                          $triptitle[] = '';
                          $triptitle[] = '';
                        }
                         $triptitle[] = 'Total Trip';
                        $busStopArr[] = $triptitle;
                         $arrdept[] = '';
                        $arrdept[] ='';
                        for($s=0;$s< 8;$s++){
                          
                          
                          $arrdept[] = 'Arrive';
                          $arrdept[] = 'Depart';

                        }
                        $busStopArr[] = $arrdept;
                        $count = 1;
                        
                        
                         foreach ($busplate as $key => $value) {
                         
                            $busprice = DB::table('bus_fares')
                            ->leftJoin('bus_registers', 'bus_fares.busfare_plateno', '=', 'bus_registers.busregister_id')
                           ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                           ->where('busregister_id','=',$value)
                           ->where('busfare_date', $startDate)
                           ->get(); 
                             
                            
                            
                                $alltime[]="";
                             foreach ($busprice as $stop) {
                                
                               
                               $alltime[] = $stop->busfare_depart;
                               $alltime[] = $stop->busfare_arrive;
                              $name = $stop->bus_plateno; 
                              $total[] = $stop->busfare_id;
                             
                             }
                             $no = count($total);
                              $num = 16 - $no;
                             $refill = $num - $no;
                            
                             for($s=1;$s<$refill;$s++){
                          
                          
                                $alltotal[] = '';
                              

                              }
                             $alltotal[] = $no;
                             $allcount[] = $count++;
                             $allname[] =$name;
                             $busStopArr[] = array_merge($allcount,$allname,$alltime,$alltotal);

                            unset($alltime);
                            unset($alltotal);
                            unset($total);
                            unset($allcount);
                            unset($allname);
                         }  
                          
                      
                       
                        Excel::create('ByTrip', function($excel) use($busStopArr) {
                          $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                              $sheet->setFontFamily('Zawgyi-One');
                              $sheet->mergeCells('A1:C1');
                              $sheet->mergeCells('A2:B2');
                              $sheet->mergeCells('A3:B3');
                              $sheet->mergeCells('A4:B4');
                              $sheet->mergeCells('D2:E2');
                              $sheet->mergeCells('D3:E3');
                              $sheet->mergeCells('C6:D6');
                             $sheet->mergeCells('E6:F6');
                             $sheet->mergeCells('G6:H6');
                             $sheet->mergeCells('I6:J6');
                             $sheet->mergeCells('K6:L6');
                             $sheet->mergeCells('M6:N6');
                             $sheet->mergeCells('O6:P6');
                             $sheet->mergeCells('Q6:R6');
                             
                              $sheet->setHeight(1, 30);
                              $sheet->setHeight(2, 20);
                              $sheet->setHeight(3, 20);
                              $sheet->setHeight(4, 20);
                              $sheet->setHeight(5, 30);
                              $sheet->setHeight(6, 25);
                              $sheet->setHeight(15);
                              $sheet->setAutoSize(true);
                              $sheet->setTitle('Detail Trip Report By Bus');
                            
                            $sheet->fromArray($busStopArr, null, 'A1', false, false);
                            $sheet->row(1, function ($row) {
                                $row->setFontWeight('bold');
                                $row->setFontSize(14);
                               

                            });
                            $sheet->setBorder('A6:S6', 'thin');
                            $sheet->row(6, function ($row) {
                                $row->setFontWeight('bold');
                                $row->setBackground('#ebebeb');
                                $row->setAlignment('center');

                            });
                                                   
                            
                          });
                        })->export($filetype);

                    }else{
                      $bus = Bus::get();
                        $company = BusCompany::get();
                        Session::flash('message', 'Sorry You export Empty Data');
                       
                        return view('admin.busfare.byTrip',compact('company','bus'));

                    }  
        
        }else{
           $bus = Bus::get();
                        $company = BusCompany::get();
                        Session::flash('message', 'All Field is required');
                       
                        return view('admin.busfare.byTrip',compact('company','bus'));
        }
    }
    public function getCharacter($string)
    {
      $expr = '/(?<=\s|^)[a-z]/i';
        preg_match_all($expr, $string, $matches);

        $result = implode('', $matches[0]);

        $result = strtoupper($result);

        return $result;
    }
    //get bus name with company
    public function getBusName(Request $request)
    {
        $company =  $request->input('comid');
        $buslist = DB::table('bus_registers')
        ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
        ->where('busCompany','=',$company)
        ->get();

        return response()->json($buslist);
    }
    public function ByGas()
    {
       $company = BusCompany::get();
        return view("admin.busfare.byGas", compact("company"));
    }
    public function ByGasExport(Request $request)
    {
        $getCompany = $request->input('getCompany');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');
        $filetype = $request->input('file_type');

         if(!empty($getCompany) || !empty($startDate) && !empty($endDate)){

            if(!empty($getCompany) && !empty($startDate) && !empty($endDate)){
                $busgas = DB::table('bus_gases')
                ->leftJoin('bus_registers', 'bus_gases.gas_plateno', '=', 'bus_registers.busregister_id')
                ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                ->leftJoin('devices', 'bus_gases.gas_deviceid', '=', 'devices.device_id')
                ->leftJoin('trips', 'bus_gases.gas_tripname', '=', 'trips.trip_id')
                ->where('company_id',$getCompany)
                ->where('gas_date','>=',$startDate)
                ->where('gas_date','<=',$endDate)
                ->get();
              if(!$busgas->isEmpty()){
                  foreach($busgas as $gas){
                    
                    $busregister[] = $gas->busregister_id;
                    $company = $gas->company_name;
                    
                  } 
                  $count=1;
     
                  $register =array_unique($busregister);
        
                        $num = count($register);
                        $currentdate = Date('d-M-Y');
                        $busStopArr[0] = ['Gas Refilled Report By Bus', '','','',''];
                        $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                        $busStopArr[] = ['Start Date : '.$startDate,'', '','End Date :'.$endDate];
                        $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                        $busStopArr[] = ['', '', '','',''];
                        $busStopArr[] = ['NO',"Plate No",'Refill-1','Refill-2','Refill-3','Refill-4','Refill-5','Refill-6','Total Unit' ,'Total Distance'];
                      foreach($register as $key=>$val)
                      {
                          $gasregister = DB::table('bus_gases')
                          ->leftJoin('bus_registers', 'bus_gases.gas_plateno', '=', 'bus_registers.busregister_id')
                          ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                          ->leftJoin('devices', 'bus_gases.gas_deviceid', '=', 'devices.device_id')
                          ->leftJoin('trips', 'bus_gases.gas_tripname', '=', 'trips.trip_id')
                          ->where('busregister_id',$val)
                          ->where('gas_date','>=',$startDate)
                          ->where('gas_date','<=',$endDate)
                          ->get();
                           $total=0;
                      $unittotal=0;
                          foreach($gasregister as $gas){
                            $total += $gas->gas_distance;
                              $plate[]= $gas->gas_unit;
                              $unittotal += $gas->gas_unit;
                              $plateNUM = $gas->bus_plateno;
                             
                          }

                         $plate1 = isset($plate['0']) ? $plate['0'] : '';
                         $plate2 = isset($plate['1']) ?  $plate['1'] : '';
                         $plate3 = isset($plate['2']) ?  $plate['2']: '';
                         $plate4 = isset($plate['3']) ?  $plate['3'] : '';
                         $plate5 = isset($plate['4']) ? $plate['4'] : '';
                         $plate6 = isset($plate['5']) ? $plate['5'] : '';
                         unset($plate);
                        $busStopArr[] = [$count++,$plateNUM,$plate1,$plate2,$plate3,$plate4,$plate5,$plate6,$unittotal,$total];
                     
                       
                
                      }
                        Excel::create('ByGas', function($excel) use($busStopArr) {
                                $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                                    $sheet->setFontFamily('Zawgyi-One');
                                    $sheet->mergeCells('A1:B1:C1');
                                    $sheet->mergeCells('A2:B2');
                                    $sheet->mergeCells('A3:B3');
                                    $sheet->mergeCells('A4:B4');
                                    $sheet->mergeCells('D2:E2');
                                    $sheet->mergeCells('D3:E3');
                                    $sheet->setHeight(1, 30);
                                    $sheet->setHeight(2, 20);
                                    $sheet->setHeight(3, 20);
                                    $sheet->setHeight(4, 20);
                                    $sheet->setHeight(5, 25);
                                    $sheet->setHeight(6, 25);
                                    $sheet->setHeight(15);
                                    $sheet->setAutoSize(true);
                                    $sheet->setTitle('Detail Trip Report By Bus');
                                  
                                  $sheet->fromArray($busStopArr, null, 'A1', false, false);
                                  $sheet->row(1, function ($row) {
                                      $row->setFontWeight('bold');
                                      $row->setFontSize(14);
                                     

                                  });
                                  $sheet->row(6, function ($row) {
                                      $row->setFontWeight('bold');
                                      $row->setBackground('#ebebeb');

                                  });
                                                         
                                  
                                });
                              })->export($filetype);
              }else{
                   $company = BusCompany::get();
                        Session::flash('message', 'Sorry You export Empty Data');
                       
                        return view('admin.busfare.byGas',compact('company'));
              }
                
            }else if(!empty($getCompany) && empty($startDate) && empty($endDate)){
               $busgas = DB::table('bus_gases')
                ->leftJoin('bus_registers', 'bus_gases.gas_plateno', '=', 'bus_registers.busregister_id')
                ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                ->leftJoin('devices', 'bus_gases.gas_deviceid', '=', 'devices.device_id')
                ->leftJoin('trips', 'bus_gases.gas_tripname', '=', 'trips.trip_id')
                ->where('company_id',$getCompany)
                ->get();
                if(!$busgas->isEmpty()){
                  foreach($busgas as $gas){
                    
                    $busregister[] = $gas->busregister_id;
                    $company = $gas->company_name;
                    
                  } 
                  $count=1;
     
                  $register =array_unique($busregister);
        
                        $num = count($register);
                        $currentdate = Date('d-M-Y');
                        $busStopArr[0] = ['Gas Refilled Report By Bus', '','','',''];
                        $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                        $busStopArr[] = ['Start Date : '.$startDate,'', '','End Date :'.$endDate];
                        $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                        $busStopArr[] = ['', '', '','',''];
                        $busStopArr[] = ['NO',"Plate No",'Refill-1','Refill-2','Refill-3','Refill-4','Refill-5','Refill-6','Total Unit' ,'Total Distance'];
                      foreach($register as $key=>$val)
                      {
                          $gasregister = DB::table('bus_gases')
                          ->leftJoin('bus_registers', 'bus_gases.gas_plateno', '=', 'bus_registers.busregister_id')
                          ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                          ->leftJoin('devices', 'bus_gases.gas_deviceid', '=', 'devices.device_id')
                          ->leftJoin('trips', 'bus_gases.gas_tripname', '=', 'trips.trip_id')
                          ->where('busregister_id',$val)
                          ->get();
                           $total=0;
                      $unittotal=0;
                          foreach($gasregister as $gas){
                            $total += $gas->gas_distance;
                              $plate[]= $gas->gas_unit;
                              $unittotal += $gas->gas_unit;
                              $plateNUM = $gas->bus_plateno;
                             
                          }

                         $plate1 = isset($plate['0']) ? $plate['0'] : '';
                         $plate2 = isset($plate['1']) ?  $plate['1'] : '';
                         $plate3 = isset($plate['2']) ?  $plate['2']: '';
                         $plate4 = isset($plate['3']) ?  $plate['3'] : '';
                         $plate5 = isset($plate['4']) ? $plate['4'] : '';
                         $plate6 = isset($plate['5']) ? $plate['5'] : '';
                         unset($plate);
                        $busStopArr[] = [$count++,$plateNUM,$plate1,$plate2,$plate3,$plate4,$plate5,$plate6,$unittotal,$total];
                     
                       
                
                      }
                        Excel::create('ByGas', function($excel) use($busStopArr) {
                                $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                                    $sheet->setFontFamily('Zawgyi-One');
                                    $sheet->mergeCells('A1:B1:C1');
                                    $sheet->mergeCells('A2:B2');
                                    $sheet->mergeCells('A3:B3');
                                    $sheet->mergeCells('A4:B4');
                                    $sheet->mergeCells('D2:E2');
                                    $sheet->mergeCells('D3:E3');
                                    $sheet->setHeight(1, 30);
                                    $sheet->setHeight(2, 20);
                                    $sheet->setHeight(3, 20);
                                    $sheet->setHeight(4, 20);
                                    $sheet->setHeight(5, 25);
                                    $sheet->setHeight(6, 25);
                                    $sheet->setHeight(15);
                                    $sheet->setAutoSize(true);
                                    $sheet->setTitle('Detail Trip Report By Bus');
                                  
                                  $sheet->fromArray($busStopArr, null, 'A1', false, false);
                                  $sheet->row(1, function ($row) {
                                      $row->setFontWeight('bold');
                                      $row->setFontSize(14);
                                     

                                  });
                                  $sheet->row(6, function ($row) {
                                      $row->setFontWeight('bold');
                                      $row->setBackground('#ebebeb');

                                  });
                                                         
                                  
                                });
                              })->export($filetype);
              }else{
                   $company = BusCompany::get();
                        Session::flash('message', 'Sorry You export Empty Data');
                       
                        return view('admin.busfare.byGas',compact('company'));
              }
            }else{
               $busgas = DB::table('bus_gases')
                ->leftJoin('bus_registers', 'bus_gases.gas_plateno', '=', 'bus_registers.busregister_id')
                ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                ->leftJoin('devices', 'bus_gases.gas_deviceid', '=', 'devices.device_id')
                ->leftJoin('trips', 'bus_gases.gas_tripname', '=', 'trips.trip_id')
                ->where('gas_date','>=',$startDate)
                ->where('gas_date','<=',$endDate)
                ->get();
                if(!$busgas->isEmpty()){
                  foreach($busgas as $gas){
                    
                    $busregister[] = $gas->busregister_id;
                    $company = '';
                    
                  } 
                  $count=1;
     
                  $register =array_unique($busregister);
        
                        $num = count($register);
                        $currentdate = Date('d-M-Y');
                        $busStopArr[0] = ['Gas Refilled Report By Bus', '','','',''];
                        $busStopArr[] = ['Company Name : '.$company, '', '','Report Date : '.$currentdate,''];
                        $busStopArr[] = ['Start Date : '.$startDate,'', '','End Date :'.$endDate];
                        $busStopArr[] = ['Total Bus Count :'.$num,'','','',''];
                        $busStopArr[] = ['', '', '','',''];
                        $busStopArr[] = ['NO',"Plate No",'Refill-1','Refill-2','Refill-3','Refill-4','Refill-5','Refill-6','Total Unit' ,'Total Distance'];
                      foreach($register as $key=>$val)
                      {
                          $gasregister = DB::table('bus_gases')
                          ->leftJoin('bus_registers', 'bus_gases.gas_plateno', '=', 'bus_registers.busregister_id')
                          ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
                          ->leftJoin('devices', 'bus_gases.gas_deviceid', '=', 'devices.device_id')
                          ->leftJoin('trips', 'bus_gases.gas_tripname', '=', 'trips.trip_id')
                          ->where('busregister_id',$val)
                          ->where('gas_date','>=',$startDate)
                          ->where('gas_date','<=',$endDate)
                          ->get();
                           $total=0;
                      $unittotal=0;
                          foreach($gasregister as $gas){
                            $total += $gas->gas_distance;
                              $plate[]= $gas->gas_unit;
                              $unittotal += $gas->gas_unit;
                              $plateNUM = $gas->bus_plateno;
                             
                          }

                         $plate1 = isset($plate['0']) ? $plate['0'] : '';
                         $plate2 = isset($plate['1']) ?  $plate['1'] : '';
                         $plate3 = isset($plate['2']) ?  $plate['2']: '';
                         $plate4 = isset($plate['3']) ?  $plate['3'] : '';
                         $plate5 = isset($plate['4']) ? $plate['4'] : '';
                         $plate6 = isset($plate['5']) ? $plate['5'] : '';
                         unset($plate);
                        $busStopArr[] = [$count++,$plateNUM,$plate1,$plate2,$plate3,$plate4,$plate5,$plate6,$unittotal,$total];
                     
                       
                
                      }
                        Excel::create('ByGas', function($excel) use($busStopArr) {
                                $excel->sheet('ExportFile', function($sheet) use($busStopArr) {
                                    $sheet->setFontFamily('Zawgyi-One');
                                    $sheet->mergeCells('A1:B1:C1');
                                    $sheet->mergeCells('A2:B2');
                                    $sheet->mergeCells('A3:B3');
                                    $sheet->mergeCells('A4:B4');
                                    $sheet->mergeCells('D2:E2');
                                    $sheet->mergeCells('D3:E3');
                                    $sheet->setHeight(1, 30);
                                    $sheet->setHeight(2, 20);
                                    $sheet->setHeight(3, 20);
                                    $sheet->setHeight(4, 20);
                                    $sheet->setHeight(5, 25);
                                    $sheet->setHeight(6, 25);
                                    $sheet->setHeight(15);
                                    $sheet->setAutoSize(true);
                                    $sheet->setTitle('Detail Trip Report By Bus');
                                  
                                  $sheet->fromArray($busStopArr, null, 'A1', false, false);
                                  $sheet->row(1, function ($row) {
                                      $row->setFontWeight('bold');
                                      $row->setFontSize(14);
                                     

                                  });
                                  $sheet->row(6, function ($row) {
                                      $row->setFontWeight('bold');
                                      $row->setBackground('#ebebeb');

                                  });
                                                         
                                  
                                });
                              })->export($filetype);
              }else{
                   $company = BusCompany::get();
                        Session::flash('message', 'Sorry You export Empty Data');
                       
                        return view('admin.busfare.byGas',compact('company'));
              }
            }

         }else{
            $company = BusCompany::get();
                Session::flash('message', 'Please Select company OR require Start Date and end Date');
               
                return view('admin.busfare.byGas',compact('company'));
         }
         
        
    }
    
}
