<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusRegister;
use App\BusCompany;
use App\Bus;
use DB;
use Session;
class BusRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $register  =  DB::table('bus_registers')
            ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
            ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
            ->get();
        return view('admin.busregister.index',compact('register'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $company = BusCompany::get();
        $bus = Bus::get();
        return view('admin.busregister.create',compact('company','bus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $data = [
        'bus_plateno'      => $request->input("bus_plateno"),
        'busName'      => $request->input("busName"),
        'busCompany'      => $request->input("busCompany"),
        'bus_manufacturer'      => $request->input("bus_manufacturer"),
        'bus_model'      => $request->input("bus_model"),
       
        ];

        $busregister = BusRegister::create($data);
        Session::flash('message', 'You have successfully Insert Bus Registration.');
        return redirect()->route('busregister.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $busregister = BusRegister::FindOrFail($id);
        $company = BusCompany::get();
        $bus = Bus::get();
        return view('admin.busregister.edit',compact('company','busregister','bus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $data = [
        'bus_plateno'      => $request->input("bus_plateno"),
        'busName'      => $request->input("busName"),
        'busCompany'      => $request->input("busCompany"),
        'bus_manufacturer'      => $request->input("bus_manufacturer"),
        'bus_model'      => $request->input("bus_model"),
       
        ];
        DB::table('bus_registers')
            ->where('busregister_id', $id)
            ->update($data);
           
        
         Session::flash('message', 'You have successfully updated Bus Registration');
        return redirect()->route('busregister.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        BusRegister::destroy($id);
        Session::flash('message', 'You have successfully deleted Bus Registration.');
        return redirect()->route("busregister.index");
    }
}
