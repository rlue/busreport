<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusCompany;
use DB;
use Session;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $company = BusCompany::get();
        return view('admin.company.index',compact('company'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = [
        'company_name'      => $request->input("company_name"),
        'contact_person'      => $request->input("contact_person"),
        'company_phone'      => $request->input("company_phone"),
        'company_mobile'      => $request->input("company_mobile"),
        ];

        $company = BusCompany::create($data);
        Session::flash('message', 'You have successfully Insert company.');
        return redirect()->route('company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $company = BusCompany::FindOrFail($id);
        return view('admin.company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = [
        'company_name'      => $request->input("company_name"),
        'contact_person'      => $request->input("contact_person"),
        'company_phone'      => $request->input("company_phone"),
        'company_mobile'      => $request->input("company_mobile"),
        ];
         DB::table('bus_companies')
            ->where('company_id', $id)
            ->update($data);
           
        
         Session::flash('message', 'You have successfully updated Company');
        return redirect()->route('company.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        BusCompany::destroy($id);
        Session::flash('message', 'You have successfully deleted company.');
        return redirect()->route("company.index");
    }
}
