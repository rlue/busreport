<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreInspector;
use App\Inspector;
use App\BusCompany;
use DB;
use Session;
class InspectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $inspector  =  DB::table('inspectors')
            ->leftJoin('bus_companies', 'inspectors.inspector_company', '=', 'bus_companies.company_id')
            ->get();
        return view('admin.inspector.index',compact('inspector'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $company = BusCompany::get();
        return view('admin.inspector.create',compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInspector $request)
    {
        //
         $data = [
        'inspector_rfid'      => $request->input("inspector_rfid"),
        'inspector_name'      => $request->input("inspector_name"),
        'inspector_nrc'      => $request->input("inspector_nrc"),
        'inspector_dob'      => $request->input("inspector_dob"),
        'inspector_company'      => $request->input("inspector_company"),
        'inspector_address'      => $request->input("inspector_address"),
        'inspector_phone'      => $request->input("inspector_phone"),
        ];

        $inspector = Inspector::create($data);
        Session::flash('message', 'You have successfully Insert inspector.');
        return redirect()->route('inspector.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $inspector = Inspector::FindOrFail($id);
        $company = BusCompany::get();
        return view('admin.inspector.edit',compact('company','inspector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $data = [
        'inspector_rfid'      => $request->input("inspector_rfid"),
        'inspector_name'      => $request->input("inspector_name"),
        'inspector_nrc'      => $request->input("inspector_nrc"),
        'inspector_dob'      => $request->input("inspector_dob"),
        'inspector_company'      => $request->input("inspector_company"),
        'inspector_address'      => $request->input("inspector_address"),
        'inspector_phone'      => $request->input("inspector_phone"),
        ];
        DB::table('inspectors')
            ->where('inspector_id', $id)
            ->update($data);
           
        
         Session::flash('message', 'You have successfully updated Inspector');
        return redirect()->route('inspector.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Inspector::destroy($id);
        Session::flash('message', 'You have successfully deleted Inspector.');
        return redirect()->route("inspector.index");
    }
}
