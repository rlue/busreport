<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Street;
use DB;
use Session;
class StreetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
         $street = Street::latest()->get();
       
        return view("admin.street.index", compact("street"));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         
        return view('admin.street.create');
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //
       $data = [
        'street_name'      => $request->input("street_name")
       
        ];

        $township = Street::create($data);
        Session::flash('message', 'You have successfully Insert Street.');
        return redirect()->route('street.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $street = Street::FindOrFail($id);
        
        return view('admin.street.edit',compact('street'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        //
        
        $township_name  = $request->input("street_name");
       
        $userupdate = ['street_name' => $township_name];
       
       
       DB::table('streets')
            ->where('id', $id)
            ->update($userupdate);
           
        
         Session::flash('message', 'You have successfully updated Street.');
        return redirect()->route('street.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Street::destroy($id);
        Session::flash('message', 'You have successfully deleted Street.');
        return redirect()->route("street.index");
       
    }
}

