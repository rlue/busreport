<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDriver;
use App\Driver;
use App\BusCompany;
use App\BusRegister;
use App\Bus;
use DB;
use Session;
class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $driver = Driver::leftJoin('bus_registers', 'drivers.bl_refno', '=', 'bus_registers.busregister_id')
        ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
        ->get();
        return view("admin.driver.index", compact("driver"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $company = BusCompany::get();
        $busregister  =  DB::table('bus_registers')
            ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
            ->get();
        return view('admin.driver.create',compact('company','busregister'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDriver $request)
    {
        //
       
         $data = [
        
        'driver_rfid'      => $request->input("driver_rfid"),
        'driver_captainName'      => $request->input("driver_captainName"),
        'driver_nrcno'      => $request->input("driver_nrcno"),
        'drivinglicence'      => $request->input("drivinglicence"),
        'driver_address'      => $request->input("driver_address"),
        'driver_blacklist'      => $request->input("driver_blacklist"),
        'bl_refno'      => $request->input("bl_refno"),
        'driver_dob' => $request->input("driver_dob"),
        'driver_phone' => $request->input("driver_phone"),
        'driver_company' => $request->input("driver_company"),
       
        ];

        $driver = Driver::create($data);
        Session::flash('message', 'You have successfully Insert Driver.');
        return redirect()->route('driver.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $driver = Driver::FindOrFail($id);
          $company = BusCompany::get();
        $busregister  =  DB::table('bus_registers')
            ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
            ->get();
        return view('admin.driver.edit',compact('driver','company','busregister'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $bl =  $request->input("driver_blacklist");
        if($bl == 1){
            $busref = 0;
        }else{
             $busref = $request->input("bl_refno");
        }

        $data = [
        'driver_rfid'      => $request->input("driver_rfid"),
        'driver_captainName'      => $request->input("driver_captainName"),
        'driver_nrcno'      => $request->input("driver_nrcno"),
        'drivinglicence'      => $request->input("drivinglicence"),
        'driver_address'      => $request->input("driver_address"),
        'driver_blacklist'      => $request->input("driver_blacklist"),
        'bl_refno'      => $busref,
        'driver_dob' => $request->input("driver_dob"),
        'driver_phone' => $request->input("driver_phone"),
        'driver_company' => $request->input("driver_company"),
       
        ];
         DB::table('drivers')
            ->where('driver_id', $id)
            ->update($data);
           
        
         Session::flash('message', 'You have successfully updated Driver.');
        return redirect()->route('driver.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Driver::destroy($id);
        Session::flash('message', 'You have successfully deleted Driver.');
        return redirect()->route("driver.index");
    }
}
