<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Township;
use DB;
use Session;
class TownshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
         $township = Township::latest()->get();
       
        return view("admin.township.index", compact("township"));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         
        return view('admin.township.create');
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //
       $data = [
        'township_name'      => $request->input("township_name")
       
        ];

        $township = Township::create($data);
        Session::flash('message', 'You have successfully Insert Township.');
        return redirect()->route('townships.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $township = Township::FindOrFail($id);
        
        return view('admin.township.edit',compact('township'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        //
        
        $township_name  = $request->input("township_name");
       
        $userupdate = ['township_name' => $township_name];
       
       
       DB::table('townships')
            ->where('id', $id)
            ->update($userupdate);
           
        
         Session::flash('message', 'You have successfully updated Township.');
        return redirect()->route('townships.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Township::destroy($id);
        Session::flash('message', 'You have successfully deleted Township.');
        return redirect()->route("townships.index");
       
    }
}

