<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bus;
use App\BusGas;
use DB;
use Session;
class BusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
         $bus = Bus::latest()->get();
       
        return view("admin.bus.index", compact("bus"));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         
        return view('admin.bus.create');
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //
       $data = [
        'bus_name'      => $request->input("bus_name")
       
        ];

        $township = bus::create($data);
        Session::flash('message', 'You have successfully Insert Bus.');
        return redirect()->route('bus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bus = Bus::FindOrFail($id);
        
        return view('admin.bus.edit',compact('bus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        //
        
        $bus_name  = $request->input("bus_name");
       
        $userupdate = ['bus_name' => $bus_name];
       
       
       DB::table('buses')
            ->where('bus_id', $id)
            ->update($userupdate);
           
        
         Session::flash('message', 'You have successfully updated Bus.');
        return redirect()->route('bus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bus::destroy($id);
        Session::flash('message', 'You have successfully deleted Bus.');
        return redirect()->route("bus.index");
       
    }

    public function busGasList()
    {
        $busgas = DB::table('bus_gases')
        ->leftJoin('bus_registers', 'bus_gases.gas_plateno', '=', 'bus_registers.busregister_id')
        ->leftJoin('bus_companies', 'bus_registers.busCompany', '=', 'bus_companies.company_id')
        ->leftJoin('devices', 'bus_gases.gas_deviceid', '=', 'devices.device_id')
        ->leftJoin('trips', 'bus_gases.gas_tripname', '=', 'trips.trip_id')
        ->get();

        return view('admin.bus.gaslist',compact('busgas'));
    }
}
