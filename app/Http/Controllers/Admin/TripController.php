<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Trip;
use App\BusStop;
use App\BusRegister;
use App\Street;
use App\Township;
use App\Bus;
use DB;
use Session;
class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $trip =  Trip::with('bus')->get();
         $bus = BusStop::get();
         
        

        return view("admin.trip.index", compact("trip"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $trip =  Trip::get();
        $bus = Bus::get();
        $busregister = Bus::get();
            
         $busstops =json_encode(array_map([$this, 'transformItem'], BusStop::with(['township','street'])->get()->toArray()));
        $fareprice= [ '100','200','300','400'];
       
        $fare= json_encode($fareprice);


        return view("admin.trip.create", compact("busstops","fare","company","busregister"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $descrption = implode(',',$request->input("trip_description"));
        $fare = implode(',',$request->input("trip_fare"));
        $group = implode(',',$request->input("trip_group"));
       
         $data = [
        'route_name'      => $request->input("route_name"),
        'trip_bus'      => $request->input('trip_bus'),
        'trip_description'      => $descrption,
        'trip_fare'      => $fare,
        'trip_group'    => $group,
       
        ];
       
        $busregister = Trip::create($data);
        Session::flash('message', 'You have successfully Insert Bus Registration.');
        return redirect()->route('trip.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $trip = Trip::FindOrFail($id);

        $tripfare = explode(',', $trip['trip_fare']);
        foreach($tripfare as $key=>$val){
            $fareprices[]= $val;
        }

         $tripgroup = explode(',', $trip['trip_group']);
        foreach($tripgroup as $key=>$val){
            $group[]= $val;
        }
        $descrption = explode(',', $trip['trip_description']);
        
        foreach($descrption as $key=>$val)
        {
            
           $bus[] = BusStop::FindOrFail($val);
           
        }
        $i=0;
        foreach($bus as $b){
            $data[] = array(
            'id'         => $b['id'] ,
            'busstop_township'         => $b['busstop_township'],
            'busstop_street'         => $b['busstop_street'],
            'busstop_name'         => $b['busstop_name'],
            'busstop_lat'           => $b['busstop_lat'] ,
            'busstop_long'          => $b['busstop_long'] ,
            'busstop_code'          => $b['busstop_code'],
            'busfare_price'       => $fareprices[$i],
            'busfare_group'         => $group[$i],
                );
         $i++;
        }
        
        $serviceno = Bus::get();

        
         $busstops =json_encode(array_map([$this, 'transformItem'], BusStop::with(['township','street'])->get()->toArray()));
        $list =json_encode(array_map([$this, 'transformPrice'],$data));
        
         $fareprice= [ '100','200','300','400'];
        $fare= json_encode($fareprice);
        
        
        
        
       
        $fareTrip= json_encode($fareprices);
        
      
        return view('admin.trip.edit',compact('street','trip','busstops','list','fare','fareTrip','serviceno'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $descrption = implode(',',$request->input("trip_description"));
        $fare = implode(',',$request->input("trip_fare"));
         $group = implode(',',$request->input("trip_group"));
       
         $data = [
        'route_name'      => $request->input("route_name"),
        'trip_description'      => $descrption,
         'trip_bus'      => $request->input('trip_bus'),
        'trip_fare'      => $fare,
        'trip_group'       => $group,
        ];

         DB::table('trips')
            ->where('trip_id', $id)
            ->update($data);
           
        
         Session::flash('message', 'You have successfully updated Trip.');
        return redirect()->route('trip.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         Trip::destroy($id);
        Session::flash('message', 'You have successfully deleted Street.');
        return redirect()->route("trip.index");
       
    }
    public function getMap(Request $request)
    {
        
        $tripid = $request->input('tripId');
        $trip = Trip::FindOrFail($tripid);

        $descrption = explode(',', $trip['trip_description']);
        foreach($descrption as $key=>$val)
        {
            $bus[] = BusStop::FindOrFail($val);
        }
        
        $data = [
            'froutes' => ($bus) ? json_encode($bus) : json_encode([]),
            
        ];
        return view('admin.trip.map',compact('data'));
    }
    public function transformItem($item)
    {
        
        $township = isset($item['township']['township_name']) ? ' (' . $item['township']['township_name'] . ')' : '';
        $street = isset($item['street']['street_name']) ? ' ('. $item['street']['street_name'] . ') ' : '';
       
         $name = $item['busstop_code'] . ' - ' . $item['busstop_name'] . $township . $street;
        //$name =  $item['name'];
        return [
            'id'         => $item['id'] ,
            'value'         => $item['id'] ,
            'label'         => $name,
            'name'         => $item['busstop_name'],
            'lat'           => $item['busstop_lat'] ,
            'long'          => $item['busstop_long'] ,
            'code'          => $item['busstop_code'],
            'order'         =>  true,
            'listfixed'     =>  false
            ];
    }
    public function transformPrice($item)
    {
        
        $township = isset($item['township']['township_name']) ? ' (' . $item['township']['township_name'] . ')' : '';
        $street = isset($item['street']['street_name']) ? ' ('. $item['street']['street_name'] . ') ' : '';
       
         $name = $item['busstop_code'] . ' - ' . $item['busstop_name'] . $township . $street;
        //$name =  $item['name'];
        return [
            'id'         => $item['id'] ,
            'value'         => $item['id'] ,
            'label'         => $name,
            'name'         => $item['busstop_name'],
            'lat'           => $item['busstop_lat'] ,
            'long'          => $item['busstop_long'] ,
            'code'          => $item['busstop_code'],
            'bus_fare'      => $item['busfare_price'],
            'group'         => $item['busfare_group'],
            'order'         =>  true,
            'listfixed'     =>  false
            ];
    }

}
