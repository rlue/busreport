<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Device;
use App\BusRegister;
use DB;
use Session;
class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $device =  DB::table('devices')
            ->leftJoin('bus_registers', 'devices.plateNO', '=', 'bus_registers.busregister_id')
            ->leftJoin('buses', 'bus_registers.busName', '=', 'buses.bus_id')
            ->get();
        return view("admin.device.index", compact("device"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $busregister = BusRegister::get();
        return view('admin.device.create',compact('busregister'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = [
        'deviceID'      => $request->input("deviceID"),
        'plateNO'      => $request->input("plateNO"),
        ];
       
        $device = Device::create($data);
        Session::flash('message', 'You have successfully Insert Device.');
        return redirect()->route('device.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $device = Device::FindOrFail($id);
          $busregister = BusRegister::get();
        return view('admin.device.edit',compact('device','busregister'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $data = [
        'deviceID'      => $request->input("deviceID"),
        'plateNO'      => $request->input("plateNO"),
        
        ];
         DB::table('devices')
            ->where('device_id', $id)
            ->update($data);
           
        
         Session::flash('message', 'You have successfully updated Device.');
        return redirect()->route('device.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Device::destroy($id);
        Session::flash('message', 'You have successfully deleted Device.');
        return redirect()->route("device.index");
    }
}
