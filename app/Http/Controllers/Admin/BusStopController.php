<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\BusStop;
use App\Street;
use App\Township;
use DB;
use Session;
class BusStopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
         $busstop =  DB::table('bus_stops')
            ->leftJoin('streets', 'bus_stops.busstop_street', '=', 'streets.id')
            ->leftJoin('townships', 'bus_stops.busstop_township', '=', 'townships.id')
            ->select('bus_stops.id as busstopid','bus_stops.busstop_name','bus_stops.busstop_code','bus_stops.busstop_lat','bus_stops.busstop_long','bus_stops.busstop_street','bus_stops.busstop_township','streets.street_name','townships.township_name','bus_stops.id as busstopid')
            ->get();
       
        return view("admin.busstop.index", compact("busstop"));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         $street = Street::get();
         $township = Township::get();
        return view('admin.busstop.create',compact('street','township'));
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //
       $data = [
        'busstop_name'      => $request->input("busstop_name"),
        'busstop_code'      => $request->input("busstop_code"),
        'busstop_street'      => $request->input("busstop_street"),
        'busstop_township'      => $request->input("busstop_township"),
        'busstop_lat'      => $request->input("busstop_lat"),
        'busstop_long'      => $request->input("busstop_long"),
       
        ];
        
        $township = BusStop::create($data);
        Session::flash('message', 'You have successfully Insert Township.');
        return redirect()->route('busstop.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $busstop = BusStop::FindOrFail($id);
         $street = Street::get();
         $township = Township::get();
        return view('admin.busstop.edit',compact('busstop','street','township'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        //
        
         $data = [
        'busstop_name'      => $request->input("busstop_name"),
        'busstop_code'      => $request->input("busstop_code"),
        'busstop_street'      => $request->input("busstop_street"),
        'busstop_township'      => $request->input("busstop_township"),
        'busstop_lat'      => $request->input("busstop_lat"),
        'busstop_long'      => $request->input("busstop_long"),
       
        ];
       
       
       DB::table('bus_stops')
            ->where('id', $id)
            ->update($data);
           
        
         Session::flash('message', 'You have successfully updated BusStop.');
        return redirect()->route('busstop.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BusStop::destroy($id);
        Session::flash('message', 'You have successfully deleted BusStop.');
        return redirect()->route("busstop.index");
       
    }
    public function quickUpdate()
    {
          $input = Input::all();

         $name  = $input['name'];
         $pk    = $input['pk'];
         $value = $input['value'];
         $data = [
             $name => $value
         ];

         $updates = DB::table('bus_stops')
             ->where('id',$pk )
             ->update($data);
            return response()->json($updates);
    }

    public function streetList()
    {
        $street = Street::get();

        $data = [];

        foreach($street as $t)
        {   
            $data[] = array(
                'value' => $t->id,
                'text' => $t->street_name
                );
        }
        
        return response()->json($data);
    }

    public function townshipList()
    {
        $township = Township::get();

        $data = [];

        foreach($township as $t)
        {   
            $data[] = array(
                'value' => $t->id,
                'text' => $t->township_name
                );
        }
        
        return response()->json($data);
    }
}

