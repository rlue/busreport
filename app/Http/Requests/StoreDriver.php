<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDriver extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
        'driver_rfid'  => 'unique:drivers,driver_rfid',
        'driver_nrcno'     => 'unique:drivers,driver_nrcno',
        'drivinglicence'    => 'unique:drivers,drivinglicence'
        ];
    }
}
