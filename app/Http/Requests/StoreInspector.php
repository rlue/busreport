<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInspector extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
        'inspector_rfid'  => 'unique:inspectors,inspector_rfid',
        'inspector_nrc'     => 'unique:inspectors,inspector_nrc'
       
        ];
    }
}
