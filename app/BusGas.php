<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusGas extends Model
{
    //
    protected $table = 'bus_gases';
    protected $fillable = [
        'gas_plateno','gas_deviceid','gas_tripname','gas_unit','gas_distance','gas_date'
    ];

    protected $primaryKey = 'gas_id';
}
