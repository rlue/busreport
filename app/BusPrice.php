<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusPrice extends Model
{
    //
     protected $fillable = [
        'people_count','busStop','busstop_price','busstop_time','busfareid'
    ];

    protected $primaryKey = 'busprice_id';
}
