<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BusRegister;
use App\Device;
use App\Trip;
class BusFare extends Model
{
    //
     protected $fillable = [
        'busfare_deviceid','busfare_plateno','busfare_totalprice','busfare_tripname','busfare_arrive','busfare_depart','busfare_date'
    ];

    protected $primaryKey = 'busfare_id';

    public function BusRegister()
    {
        return $this->belongsTo(BusRegister::class,'busfare_plateno');
    }
    public function Device()
    {
        return $this->belongsTo(Device::class,'busfare_deviceid');
    }
    public function Trip()
    {
        return $this->belongsTo(Trip::class,'busfare_tripname');
    }
}
