<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BusRegister;
class BusCompany extends Model
{
    //
    protected $fillable = [
        'company_name','contact_person','company_phone','company_mobile'
    ];
    protected $primaryKey = 'company_id';

   
}
