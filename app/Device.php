<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BusRegister;
use App\Trip;
use App\BusCompany;
class Device extends Model
{
    //
    protected $fillable = [
        'deviceID','plateNO'
    ];
    protected $primaryKey = 'device_id';

    public function BusRegister()
    {
        return $this->belongsTo(BusRegister::class,'plateNO');
    }
     public function Trip()
    {
        return $this->belongsTo(Trip::class,'plateNO');
    }
   

}
