<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BusCompany;
class Inspector extends Model
{
    //
     protected $fillable = [
        'inspector_rfid','inspector_name','inspector_nrc','inspector_dob','inspector_company','inspector_address','inspector_phone'
    ];
    protected $primaryKey = 'inspector_id';

     public function Company()
    {
        return $this->belongsTo(BusCompany::class,'inspector_company');
    }
}
