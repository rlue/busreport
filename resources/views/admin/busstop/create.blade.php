@extends('layouts.admin')

@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@stop

@section('content')
 
    <!-- Main content -->
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Create Street</h2>
                
                           
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
              <form id="demo-form2" method="post" action="{{route('busstop.store')}}" class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="street_name">Name</label>
                        <input type="text" class='form-control' name="busstop_name" value="{{ old('street_name') }}" required="required" placeholder="Name">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="street_name">Code</label>
                        <input type="text" class='form-control' name="busstop_code" value="{{ old('street_name') }}" required="required" placeholder="Code">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="street_name">Street</label>
                        <select class="form-control" id="busstop_street" name="busstop_street">
                         <option value="">Select Street</option>
                          @foreach($street as $t)
                          
                            <option value="{{$t->id}}">{{$t->street_name}}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="street_name">Township</label>
                        <select class="form-control" id="busstop_township" name="busstop_township">
                        <option value="">Select Township</option>
                          @foreach($township as $t)
                            
                            <option value="{{$t->id}}">{{$t->township_name}}</option>
                          @endforeach

                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12 ">
                        <label for="street_name">Lat</label>
                        <input type="text" class='form-control' name="busstop_lat" id="busstop_lat" value="16.79938513" required="required" >
                    </div>
                   
                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label for="street_name">Long</label>
                        <input type="text" class='form-control' name="busstop_long" id="busstop_long" value="96.17587209" required="required" >
                    </div>
                     <div class="form-group">
                     
                       <a href="#" id="pick-location" data-target="#us6-dialog" data-toggle="modal" class="btn btn-primary" style="margin-top: 23px;"><i class="fa fa-map-marker"></i> Pick</a>
                    </div>

                        <div class="ln_solid"></div>
                        <div class="form-group ">
                         <a href="{{route('busstop.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </form>
                <div id="us6-dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Pick Location</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" style="width: 550px">
                        <div class="form-group col-sm-6">
                            <label class=" control-label">Lat:</label>
                            <input type="text" class="form-control" id="busstop-lat" />
                            
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label">Long:</label>
                            <input type="text" class="form-control" id="busstop-lon" />
                            
                        </div>
                        <div id="us3" style="width: 100%; height: 400px;"></div>
                        <div class="clearfix">&nbsp;</div>
                        
                        <div class="clearfix"></div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-add-location" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
            </div>
            </div>
        </div>
    </div>
            
                
@endsection


@section('busstopscript')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
      <script src="{{ asset('js/select2.min.js') }}"></script>
      <script  src='http://maps.google.com/maps/api/js?key=AIzaSyCtuO9pffiWDVDCCnSjznysO7sbriGJBdM&libraries=places'></script>

      <script type="text/javascript" src="{{ asset('js/locationpicker.jquery.min.js') }}"></script>
    <script>
    $('#busstop_township').select2({
     });

      $('#busstop_street').select2({
     });

                                $('#us3').locationpicker({
                                    location: {
                                        latitude: $('#busstop_lat').val(),
                                        longitude: $('#busstop_long').val()
                                    },
                                    radius: 300,
                                    inputBinding: {
                                        latitudeInput: $('#busstop-lat'),
                                        longitudeInput: $('#busstop-lon')
                                        
                                    },
                                    enableAutocomplete: true
                   
                                });
                                $('#us6-dialog').on('shown.bs.modal', function () {
                                    $('#us3').locationpicker('autosize');
                                });


                                $('#btn-add-location').on('click', function(e) {
                                
                                  $('#busstop_lat').val($('#busstop-lat').val());
                                  $('#busstop_long').val($('#busstop-lon').val());
                                  $('#us6-dialog').modal('hide');
                                });

                            </script>   
@stop