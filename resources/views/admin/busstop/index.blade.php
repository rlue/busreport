@extends('layouts.admin')

@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-editable.css') }}" rel="stylesheet">
@stop

@section('content')

            <div class="page-title">
              <div class="title_left">

                <h3>BusStop Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
               
              </div>
            </div>

            <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>BusStop List </h2>
                <div class="pull-right">
                @can('created')
               <a href="{{route('busstop.create')}}" class="btn btn-primary" >
                 <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add
                </a>
                @endcan
                </div>
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="table table-bordered" id="datatable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Street</th>
                                        <th>Township</th>
                                        <th>Lat</th>
                                        <th>Long</th>
                                        <th>Option</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($busstop as $u)
                                        <tr>
                                            
                                            <td>
                                            <a href="#" class="name_edit" data-type="text" data-pk="{{$u->busstopid}}" data-name="busstop_name" data-url="{{route('busUpdate')}}" data-original-title="Enter Name">{{$u->busstop_name}}</a>
                                            </td>
                                            <td>
                                            <a href="#" class="code_edit" data-type="text" data-pk="{{$u->busstopid}}" data-name="busstop_code" data-url="/busUpdate" data-original-title="Enter Name">
                                            {{$u->busstop_code}}</a></td>
                                            <td>
                                              <a href="#" class="street_edit" data-street-id="{{$u->busstop_street}}" data-type="select" data-pk="{{$u->busstopid}}" data-name="busstop_street" data-url="/busUpdate" data-original-title="Enter Name">
                                            {{$u->street_name}}
                                            </a>
                                            </td>
                                            <td>
                                          <a href="#" class="township_edit" data-township-id="{{$u->busstop_township}}" data-type="select" data-pk="{{$u->busstopid}}" data-name="busstop_township" data-url="/busUpdate" data-original-title="Enter Name">
                                            {{$u->township_name}}
                                            </a>
                                            </td>
                                            <td><a href="#" class="lat_edit" data-type="text" data-pk="{{$u->busstopid}}" data-name="busstop_lat" data-url="/busUpdate" data-original-title="Enter Name">{{$u->busstop_lat}}</a>
                                            </td>
                                            <td>
                                            <a href="#" class="long_edit" data-type="text" data-pk="{{$u->busstopid}}" data-name="busstop_long" data-url="/busUpdate" data-original-title="Enter Name">
                                            {{$u->busstop_long}}
                                            </a>
                                            </td>
                                            

                                            <td>
                                            <a class="btn btn-success" href="http://maps.google.com/maps?q={{$u->busstop_lat}},{{$u->busstop_long}}" target="_blank"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Map</a>
                                            @can('Update')
                                                <a class="btn btn-primary" href="{{route('busstop.edit',$u->busstopid)}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
                                                @endcan
                                                @can('delete')
                                                <a class="btn btn-danger remove_item" href="{{route('busstop.delete',$u->busstopid)}}"><span class="glyphicon glyphicon-trash
" aria-hidden="true"></span> Delete</a>
@endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
            </div>
            </div>
        </div>
    </div>
 
@endsection


@section('busstopscript')

      <script src="{{ asset('js/select2.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/bootstrap-editable.js') }}"></script>
      <script type="text/javascript">
      $.fn.editable.defaults.mode = 'inline';


          $('#datatable').on('click','.name_edit,.code_edit,.lat_edit,.long_edit', function(e){
     e.preventDefault();
     $(this).editable({
        success: function(response, newValue) {
           
        }
     })
    
  });
    
    function getStreets() {
    var url = "/street/list";
    return $.ajax({
        type:  'GET',
        async: true,
        url:   url,
        dataType: "json"
    });
  };

  getStreets().done(function(result) {
      
    $('#datatable').on('click','.street_edit', function(e){
       e.preventDefault();
       var id = $(this).data('street-id');
       $(this).editable({
          value: id,    
          source: result,
       });
    });

  });

  function getTownships() {
    var url = "/township/list";
    return $.ajax({
        type:  'GET',
        async: true,
        url:   url,
        dataType: "json"
    });
  };

  getTownships().done(function(result) {

    $('#datatable').on('click','.township_edit', function(e){
       e.preventDefault();
       var id = $(this).data('township-id');
       $(this).editable({
          value: id,    
          source: result,
       });
    });

  });


      </script>
   <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
 </script>

@stop
