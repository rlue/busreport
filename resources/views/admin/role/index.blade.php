@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
       
            <div class="page-title">
              <div class="title_left">

                <h3>Roles Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Manage <small>Roles</small></h2>
                    <a href="{{route('roles.create')}}" class="btn btn-primary pull-right">Create Role</a>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Guard Name</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($roles as $r)
                        <tr>
                          <td>{{$r->name}}</td>
                          <td>{{$r->guard_name}}</td>
                          <td><a href="{{route('roles.edit',$r->id)}}" class="btn btn-primary">Edit</a></td>
                          <td><a href="{{route('roles.delete',$r->id)}}" class="btn btn-danger">Delete</a></td>
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
        
@endsection


