@extends('layouts.admin')

@section('content')

            <div class="page-title">
              <div class="title_left">

                <h3>Bus Gas Refill Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
               
              </div>
            </div>

            <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Bus Gas Refill Data </h2>
               
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="table table-bordered" id="datatable">
                                    <thead>
                                    <tr>
                                        <th>Plate No</th>
                                        <th>Device ID</th>
                                        <th>Company</th>
                                        <th>Trip Name</th>
                                        <th>Gas Unit</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($busgas as $u)
                                        <tr>
                                            
                                            <td>{{$u->bus_plateno}}</td>
                                            <td>{{$u->deviceID}}</td>
                                            <td>{{$u->company_name}}</td>
                                            <td>{{$u->route_name}}</td>
                                            <td>{{$u->gas_unit}}</td>
                                            <td>{{$u->gas_date}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
            </div>
            </div>
        </div>
    </div>
 
@endsection



