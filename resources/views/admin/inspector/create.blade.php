@extends('layouts.admin')

@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop

@section('content')
 
    <!-- Main content -->
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Create Device </h2>
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
              <form id="demo-form2" method="post" action="{{route('inspector.store')}}" class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="inspector_rfid">RFID ID</label>
                        <input type="text" class='form-control' name="inspector_rfid" value="{{ old('inspector_rfid') }}" required="required" placeholder="RFID ID">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                        <label for="inspector_name">Inspector Name</label>
                        <input type="text" class='form-control' name="inspector_name" value="{{ old('inspector_name') }}" required="required" placeholder="Inspector Name">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                        <label for="inspector_nrc">NRC</label>
                        <input type="text" class='form-control' name="inspector_nrc" value="{{ old('inspector_nrc') }}" required="required" placeholder="NRC">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                        <label for="inspector_dob">Date of Birth</label>
                        <input type="text" class='form-control' id="inspector_dob" name="inspector_dob" value="{{ old('inspector_dob') }}" required="required" placeholder="Date of Birth">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="inspector_company">Company</label>
                        <select name="inspector_company" id="inspector_company" class="form-control">
                          <option value="0">Select Company</option>
                          @foreach($company as $b)
                            <option value="{{$b->company_id}}">{{$b->company_name}}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="inspector_phone">Contact No</label>
                        <input type="text" class='form-control' name="inspector_phone" value="{{ old('inspector_phone') }}" required="required" placeholder="Contact No">
                    </div>
                   <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label for="plateNO">Inspector Address</label>
                       <textarea name="inspector_address" class="form-control">{{ old('inspector_address')}}</textarea>
                    </div>
                    
                        
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="ln_solid"></div>
                         <a href="{{route('inspector.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div">
                </form>
            </div>
            </div>
        </div>
    </div>
            
                
           
    
@endsection

@section('busstopscript')

   
      <script src="{{ asset('js/select2.min.js') }}"></script>
      <script src="{{ asset('js/moment.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
    $('#inspector_company').select2({
     });

    </script>   
    <script type="text/javascript">
  $(function() {
   $('#inspector_dob').datetimepicker({
     format: 'DD/MM/YYYY'
   });
  });
</script>
@stop