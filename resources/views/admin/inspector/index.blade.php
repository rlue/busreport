@extends('layouts.admin')

@section('content')

            <div class="page-title">
              <div class="title_left">

                <h3>Inspector Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
               
              </div>
            </div>

            <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Inspector List </h2>
                <div class="pull-right">
                @can('created')
               <a href="{{route('inspector.create')}}" class="btn btn-primary" >
                 <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add
                </a>
                @endcan
                </div>
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="table table-bordered" id="datatable">
                                    <thead>
                                    <tr>
                                        <th>RFID ID</th>
                                        <th>Inspector Name</th>
                                        <th>NRC</th>
                                        <th>Date of Birth</th>
                                        <th>Company</th>
                                        <th>Address</th>
                                        <th>Contact No</th>
                                        <th>Option</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($inspector as $u)
                                        <tr>
                                            
                                            <td>{{$u->inspector_rfid}}</td>
                                            <td>{{$u->inspector_name}}</td>
                                            <td>{{$u->inspector_nrc}}</td>
                                            <td>{{$u->inspector_dob}}</td>
                                            <td>{{$u->company_name}}</td>
                                            <td>{{$u->inspector_address}}</td>
                                            <td>{{$u->inspector_phone}}</td>
                                            <td>
                                            @can('Update')
                                                <a class="btn btn-primary" href="{{route('inspector.edit',$u->inspector_id)}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
                                                @endcan
                                                @can('delete')
                                                <a class="btn btn-danger remove_item" href="{{route('inspector.delete',$u->inspector_id)}}"><span class="glyphicon glyphicon-trash
" aria-hidden="true"></span> Delete</a>
@endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
            </div>
            </div>
        </div>
    </div>
 
@endsection



