@extends('layouts.admin')
@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop
@section('content')


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Bus Captain Edit </h2>
                
                        
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                <form method="post" action="{{route('driver.update',$driver->driver_id)}}">
                    {{ csrf_field() }}
                   <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="driver_rfid">RFID</label>
                        <input type="text" class='form-control' name="driver_rfid" value="{{$driver->driver_rfid }}" required="required" placeholder="RFID">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="driver_captainName">Captain Name</label>
                        <input type="text" class='form-control' name="driver_captainName" value="{{$driver->driver_captainName }}" required="required" placeholder="Captain Name">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="driver_nrcno">NRC</label>
                        <input type="text" class='form-control' name="driver_nrcno" value="{{$driver->driver_nrcno }}" required="required" placeholder="NRC">
                    </div>
                     <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="driver_dob">Date of Birth</label>
                        <input type="text" class='form-control' id="driver_dob" name="driver_dob" value="{{$driver->driver_dob }}" required="required" placeholder="Date of Birth">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="drivinglicence">Driving Licence</label>
                        <input type="text" class='form-control' name="drivinglicence" value="{{$driver->drivinglicence }}" required="required" placeholder="Driving Licence">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="driver_phone">Contact No</label>
                        <input type="text" class='form-control' name="driver_phone" value="{{$driver->driver_phone }}" required="required" placeholder="Contact No">
                    </div>
                     <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="driver_company">Company</label>
                        <select name="driver_company" id="driver_company" class="form-control">
                          <option value="0">Select Company</option>
                          @foreach($company as $b)
                            @if($b->company_id == $driver->driver_company)
                             <option value="{{$b->company_id}}" selected="selected">{{$b->company_name}}</option>
                            @else
                             <option value="{{$b->company_id}}">{{$b->company_name}}</option>
                            @endif
                           
                          @endforeach
                        </select>
                    </div>
                     <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="driver_address">Address</label>
                        <textarea class="form-control" name="driver_address">{{$driver->driver_address }}</textarea>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="driver_blacklist">Black List</label>
                       @if($driver->driver_blacklist == 0)
                        <?php $yes = "checked"; ?>
                        <?php $no = ""; ?>
                       @else
                           <?php $yes = ""; ?>
                        <?php $no = "checked"; ?>
                       @endif
                    </div>
                    <div class="form-group" id="bl_condition">
                      <label class="radio-inline">
                        <input type="radio" name="driver_blacklist" id="inlineRadio2" {{$yes}} value="0"> Yes
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="driver_blacklist" id="inlineRadio3" {{$no}} value="1"> NO
                      </label>
                    </div>
                    </div>
                   <div class="form-group col-md-6 col-sm-6 col-xs-12" id="bl_bus">
                        <label for="bl_refno">BL REFNO</label>
                        <select name="bl_refno" id="bl_refno" class="form-control">
                          <option value="0">Select BusLine</option>
                          @foreach($busregister as $bus)
                            @if($bus->busregister_id == $driver->bl_refno)
                               <option value="{{$bus->busregister_id}}" selected="selected">{{$bus->bus_plateno}}/{{$bus->bus_name}}</option>
                            @else
                                 <option value="{{$bus->busregister_id}}">{{$bus->bus_plateno}} / {{$bus->bus_name}}</option>
                            @endif
                           
                          @endforeach
                        </select>
                    </div>
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="ln_solid"></div>
                    <form action="{{ route('driver.store', $driver->driver_id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                     <a href="{{route('driver.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                    </div>
                </form>
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection
            
@section('busstopscript')
<script src="{{ asset('js/select2.min.js') }}"></script>
      <script src="{{ asset('js/moment.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
      <script type="text/javascript">
        $(document).ready(function(){
          
          var blbus = $('input[name=driver_blacklist]:checked').val();
          if(blbus == 0){
               $('#bl_bus').show();
          }else{
              $('#bl_bus').hide();
          }
          $('#bl_condition input').on('change',function(){
            var blno = $('input[name=driver_blacklist]:checked').val();
             if(blno == 0){
                $('#bl_bus').show();
             }else{
                $('#bl_bus').hide();
             }

          });
           
         
        });
      </script>
    <script>
    $('#driver_company').select2({
     });
     $('#bl_refno').select2({
     });
    </script>   
    <script type="text/javascript">
  $(function() {
   $('#driver_dob').datetimepicker({
     format: 'YYYY-MM-DD'
   });
  });
</script>
@stop       

