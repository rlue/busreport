@extends('layouts.admin')

@section('content')


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Company Edit </h2>
                
                         
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

              
                <form method="post" action="{{route('company.update',$company->company_id)}}">
                    {{ csrf_field() }}
                   <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="company_name">Company Name</label>
                        <input type="text" class='form-control' name="company_name" value="{{ $company->company_name}}" required="required" placeholder="Company  Name">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                        <label for="contact_person">Contact Person</label>
                        <input type="text" class='form-control' name="contact_person" value="{{ $company->contact_person}}" required="required" placeholder="Contact Person">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="company_phone">Tel NO</label>
                        <input type="text" class='form-control' name="company_phone" value="{{ $company->company_phone}}"  placeholder="Tel Number">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="company_mobile">Mobile No</label>
                        <input type="text" class='form-control' name="company_mobile" value="{{ $company->company_mobile}}"  placeholder="Mobil No">
                    </div>
                   
                   <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="ln_solid"></div>
                    <form action="{{ route('company.store', $company->company_id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                     <a href="{{route('company.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                    </div>
                </form>
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection
            
           
