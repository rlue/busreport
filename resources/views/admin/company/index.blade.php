@extends('layouts.admin')

@section('content')

            <div class="page-title">
              <div class="title_left">

                <h3>Bus Operator Company Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
               
              </div>
            </div>

            <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Company List </h2>
                <div class="pull-right">
                @can('created')
               <a href="{{route('company.create')}}" class="btn btn-primary" >
                 <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add
                </a>
                @endcan
                </div>
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="table table-bordered" id="datatable">
                                    <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Contact Person</th>
                                        <th>Tel No</th>
                                        <th>Mobile No</th>
                                        <th>Option</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($company as $u)
                                        <tr>
                                            
                                            <td>{{$u->company_name}}</td>
                                            <td>{{$u->contact_person}}</td>
                                            <td>{{$u->company_phone}}</td>
                                            <td>{{$u->company_mobile}}</td>
                                            
                                            <td>
                                            @can('Update')
                                                <a class="btn btn-primary" href="{{route('company.edit',$u->company_id)}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
                                                @endcan
                                                @can('delete')
                                                <a class="btn btn-danger remove_item" href="{{route('company.delete',$u->company_id)}}"><span class="glyphicon glyphicon-trash
" aria-hidden="true"></span> Delete</a>
@endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
            </div>
            </div>
        </div>
    </div>
 
@endsection



