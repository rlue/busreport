@extends('layouts.admin')

@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@stop

@section('content')
 
    <!-- Main content -->
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Create Company </h2>
                
                           
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
              <form id="demo-form2" method="post" action="{{route('company.store')}}" class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="company_name">Company Name</label>
                        <input type="text" class='form-control' name="company_name" value="{{ old('company_name') }}" required="required" placeholder="Company  Name">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                        <label for="contact_person">Contact Person</label>
                        <input type="text" class='form-control' name="contact_person" value="{{ old('contact_person') }}" required="required" placeholder="Contact Person">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="company_phone">Tel NO</label>
                        <input type="text" class='form-control' name="company_phone" value="{{ old('company_phone') }}"  placeholder="Tel Number">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="company_mobile">Mobile No</label>
                        <input type="text" class='form-control' name="company_mobile" value="{{ old('company_mobile') }}"  placeholder="Mobil No">
                    </div>
                   
                    
        
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="ln_solid"></div>
                         <a href="{{route('company.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div">
                </form>
            </div>
            </div>
        </div>
    </div>
            
                
           
    
@endsection

@section('busstopscript')

   
      <script src="{{ asset('js/select2.min.js') }}"></script>
      
    <script>
    $('#device_bus').select2({
     });

    </script>   
@stop