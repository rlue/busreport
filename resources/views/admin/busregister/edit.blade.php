@extends('layouts.admin')
@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop
@section('content')


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Edit Bus Registration </h2>
                
                       
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

             
                <form method="post" action="{{route('busregister.update',$busregister->busregister_id)}}">
                    {{ csrf_field() }}
                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label for="bus_plateno">License PlateNo</label>
                        <input type="text" class='form-control' name="bus_plateno" value="{{$busregister->bus_plateno }}" required="required" placeholder="License PlateNo">
                    </div>
                     <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label for="bus_manufacturer">Manufacture</label>
                        <input type="text" class='form-control' name="bus_manufacturer" value="{{$busregister->bus_manufacturer }}" required="required" placeholder="Manufacture">
                    </div>
                     <div class="form-group col-md-4 col-sm-4 col-xs-12 ">
                        <label for="bus_model">Model Year</label>
                        <input type="text" class='form-control' name="bus_model" value="{{$busregister->bus_model }}" required="required" placeholder="Model Year">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="busName">Service Number</label>
                        <select name="busName" id="busName" class="form-control">
                          <option value="0">Select Service Number</option>
                          @foreach($bus as $b)
                            @if($b->bus_id == $busregister->busName)
                              <option value="{{$b->bus_id}}" selected="selected">{{$b->bus_name}}</option>
                            @else
                              <option value="{{$b->bus_id}}">{{$b->bus_name}}</option>
                            @endif
                            
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="busCompany">Company</label>
                        <select name="busCompany" id="busCompany" class="form-control">
                          <option value="0">Select Company</option>
                          @foreach($company as $b)
                            @if($b->company_id == $busregister->busCompany)
                              <option value="{{$b->company_id}}" selected="selected">{{$b->company_name}}</option>
                            @else
                              <option value="{{$b->company_id}}">{{$b->company_name}}</option>
                            @endif
                            
                          @endforeach
                        </select>
                    </div>
                    
                   <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="ln_solid"></div>
                    <form action="{{ route('busregister.store', $busregister->busregister_id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                     <a href="{{route('busregister.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                    </div>
                </form>
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection
            
           
@section('busstopscript')

   
     <script src="{{ asset('js/select2.min.js') }}"></script>
      <script src="{{ asset('js/moment.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
    $('#busName').select2({
     });
     $('#busCompany').select2({
     });
    </script>   
    <script type="text/javascript">
  $(function() {
   $('#inspector_dob').datetimepicker({
     format: 'DD/MM/YYYY'
   });
  });
</script> 
@stop
