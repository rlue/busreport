@extends('layouts.admin')

@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">

@stop

@section('content')
 
    <!-- Main content -->
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Bus Registration Form </h2>
                
                           
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
              <form id="demo-form2" method="post" action="{{route('busregister.store')}}" class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label for="bus_plateno">License PlateNo</label>
                        <input type="text" class='form-control' name="bus_plateno" value="{{ old('bus_plateno') }}" required="required" placeholder="License PlateNo">
                    </div>
                     <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label for="bus_manufacturer">Manufacturer</label>
                        <input type="text" class='form-control' name="bus_manufacturer" value="{{ old('bus_manufacturer') }}" required="required" placeholder="Manufacturer">
                    </div>
                     <div class="form-group col-md-4 col-sm-4 col-xs-12 ">
                        <label for="bus_model">Model Year</label>
                        <input type="text" class='form-control' name="bus_model" value="{{ old('bus_model') }}" required="required" placeholder="Model Year">
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="busName">Service Number</label>
                        <select name="busName" id="busName" class="form-control">
                          <option value="0">Select Service Number</option>
                          @foreach($bus as $b)
                            <option value="{{$b->bus_id}}">{{$b->bus_name}}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="busCompany">Company</label>
                        <select name="busCompany" id="busCompany" class="form-control">
                          <option value="0">Select Company</option>
                          @foreach($company as $b)
                            <option value="{{$b->company_id}}">{{$b->company_name}}</option>
                          @endforeach
                        </select>
                    </div>
                   
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="ln_solid"></div>
                         <a href="{{route('busregister.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div">
                </form>
            </div>
            </div>
        </div>
    </div>
            
                
           
    
@endsection

@section('busstopscript')

   
      <script src="{{ asset('js/select2.min.js') }}"></script>
     
    <script>
    $('#busName').select2({
     });
    $('#busCompany').select2({
     });
    </script>   
   
@stop