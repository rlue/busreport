@extends('layouts.admin')

@section('content')

            <div class="page-title">
              <div class="title_left">

                <h3>Bus Registration Data<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
               
              </div>
            </div>

            <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Bus Register </h2>
                <div class="pull-right">
                @can('created')
               <a href="{{route('busregister.create')}}" class="btn btn-primary" >
                 <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add
                </a>
                @endcan
                </div>
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="table table-bordered" id="datatable">
                                    <thead>
                                    <tr>
                                        <th>Licence Plate No</th>
                                        <th>Service Name</th>
                                        <th>Company Name</th>
                                        <th>Manufacture</th>
                                        <th>Model Year</th>
                                        <th>Option</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($register as $u)
                                        <tr>
                                            
                                            <td>{{$u->bus_plateno}}</td>
                                            <td>{{$u->bus_name}}</td>
                                            <td>{{$u->company_name}}</td>
                                            <td>{{$u->bus_manufacturer}}</td>
                                            <td>{{$u->bus_model}}</td>
                                            
                                            <td>
                                            @can('Update')
                                                <a class="btn btn-primary" href="{{route('busregister.edit',$u->busregister_id)}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
                                                @endcan
                                                @can('delete')
                                                <a class="btn btn-danger remove_item" href="{{route('busregister.delete',$u->busregister_id)}}"><span class="glyphicon glyphicon-trash
" aria-hidden="true"></span> Delete</a>
@endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
            </div>
            </div>
        </div>
    </div>
 
@endsection



