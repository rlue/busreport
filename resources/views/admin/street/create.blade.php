@extends('layouts.admin')

@section('content')
 
    <!-- Main content -->
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Create Street</h2>
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
              <form id="demo-form2" method="post" action="{{route('street.store')}}" class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="street_name">Street Name</label>
                        <input type="text" class='form-control' name="street_name" value="{{ old('street_name') }}" required="required" placeholder="Street Name">
                    </div>
                    
                        <div class="ln_solid"></div>
                      <a href="{{route('street.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a>    
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                </form>
            </div>
            </div>
        </div>
    </div>
            
                
           
    
@endsection
