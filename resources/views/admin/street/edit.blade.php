@extends('layouts.admin')

@section('content')


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Edit Street </h2>
                
                     <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>       
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                <form method="post" action="{{route('street.update',$street->id)}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Street Name</label>
                        <input type="text" class='form-control' name="street_name" value="{{$street->street_name}}" placeholder="Street Name " required="required">
                    </div>
                   
                        <div class="ln_solid"></div>
                    <form action="{{ route('street.store', $street->id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                     <a href="{{route('street.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                </form>
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection
            
           

