@extends('layouts.admin')

@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-editable.css') }}" rel="stylesheet">
@stop

@section('content')

            <div class="page-title">
              <div class="title_left">

                <h3>Trip Builder<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
               
              </div>
            </div>

            <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Trip List </h2>
                <div class="pull-right">
                @can('created')
               <a href="{{route('trip.create')}}" class="btn btn-primary" >
                 <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add
                </a>
                @endcan
                </div>
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="table table-bordered" id="datatable">
                                    <thead>
                                    <tr>
                                        <th>Route Name</th>
                                        <th>Route Description</th>
                                        <th>Bus Service Number</th>
                                        <th>Map</th>
                                       
                                        <th>Option</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($trip as $u)
                                        <tr>
                                            
                                           <td>{{$u->route_name}}</td>
                                           <td>
                                              <?php $busstop = explode(',',$u->trip_description);

                                                
                                                //get first value from array
                                                  $first = reset($busstop);
                                                 $firsttop = DB::table('bus_stops')->where('id','=',$first)->get();
                                                 foreach ($firsttop as $top) {
                                                    $busfirst = $top->busstop_name;
                                                 }
                                                 //get middle value from array
                                                  $i = round(count($busstop)/2) - 1;
                                                $keys = array_keys($busstop);
                                               
                                                $middlebus = ($busstop[$keys[$i]]);
                                                 $middletop = DB::table('bus_stops')->where('id','=',$middlebus)->get();
                                                 foreach ($middletop as $mid) {
                                                    $busmiddle = $mid->busstop_name;
                                                 }
                                                 //get last value from array
                                                  $last = end($busstop);
                                                 $lasttop = DB::table('bus_stops')->where('id','=',$last)->get();
                                                 foreach ($lasttop as $last) {
                                                    $buslast = $last->busstop_name;
                                                 }
                                                 echo $busfirst .' - '.$busmiddle.' - ' .$buslast ;
                                                
                                              ?>
                                             
                                             

                                           </td>
                                           <td>
                                            @if( ! empty($u->bus))
                                                {{$u->bus->bus_name}}
                                            @else

                                            @endif
                                           </td>
                                           <td>
                                           <form action="{{route('trip.map')}}" method="post" target="black">
                                           {{ csrf_field() }}
                                           <input type="hidden" name="tripId" value="{{$u->trip_id}}">
                                           <button type="submit" class="btn btn-primary">View Map</button>
                                           </form>
                                           </td>
                                            <td>
                                           @can('Update')
                                                <a class="btn btn-primary" href="{{route('trip.edit',$u->trip_id)}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
                                            @endcan
                                            @can('delete')
                                                <a class="btn btn-danger remove_item" href="{{route('trip.delete',$u->trip_id)}}"><span class="glyphicon glyphicon-trash
" aria-hidden="true"></span> Delete</a>@endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
            </div>
            </div>
        </div>
    </div>
 
@endsection


@section('busstopscript')

      <script src="{{ asset('js/select2.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/bootstrap-editable.js') }}"></script>
      <script type="text/javascript">
      $.fn.editable.defaults.mode = 'inline';


          $('#datatable').on('click','.name_edit,.code_edit,.lat_edit,.long_edit', function(e){
     e.preventDefault();
     $(this).editable({
        success: function(response, newValue) {
           
        }
     })
    
  });
    
    function getStreets() {
    var url = "/street/list";
    return $.ajax({
        type:  'GET',
        async: true,
        url:   url,
        dataType: "json"
    });
  };

  getStreets().done(function(result) {
      
    $('#datatable').on('click','.street_edit', function(e){
       e.preventDefault();
       var id = $(this).data('street-id');
       $(this).editable({
          value: id,    
          source: result,
       });
    });

  });

  function getTownships() {
    var url = "/township/list";
    return $.ajax({
        type:  'GET',
        async: true,
        url:   url,
        dataType: "json"
    });
  };

  getTownships().done(function(result) {

    $('#datatable').on('click','.township_edit', function(e){
       e.preventDefault();
       var id = $(this).data('township-id');
       $(this).editable({
          value: id,    
          source: result,
       });
    });

  });


      </script>
   <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
 </script>

@stop
