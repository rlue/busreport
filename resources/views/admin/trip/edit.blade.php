@extends('layouts.admin')

@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@stop

@section('content')
 
    <!-- Main content -->
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        <div id="app">
        <div class="x_panel">
            <div class="x_title">
                  <h2>Create Street @{{message}}</h2>
                
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
              
              <form id="demo-form2" method="post" action="{{route('trip.update',$trip->trip_id)}}" class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                    <div class="row">
                    
                      <div class="form-group  col-md-6 col-sm-6 col-xs-12">
                          <label for="route_name">Route Name</label>
                         
                          <input type="text" class='form-control' name="route_name" value="{{ $trip->route_name }}" required="required" placeholder="Route Name">
                          
                      </div>
                      <div class="form-group  col-md-6 col-sm-6 col-xs-12">
                          <label for="route_name">Service Number</label>
                          
                          <select name="trip_bus"  class="form-control">
                            <option value="0">Choose Service Number</option>
                            @foreach($serviceno as $b)
                            @if($b->bus_id == $trip->trip_bus)
                               <option value="{{$b->bus_id}}" selected="selected">{{$b->bus_name}}</option>
                            @else
                                 <option value="{{$b->bus_id}}">{{$b->bus_name}} </option>
                            @endif
                             
                            @endforeach
                          </select>
                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-md-9 col-sm-9 col-xs-12">
                        <label for="bus_stops">Forwards Bus Stops</label>
                         <v-select :options="busstops" v-model="item" placeholder="Select Bus Stop"></v-select>
                      </div>
                      <div class="col-md-3 margin-top-30">
                         <button class="btn btn-primary btn-sm" v-on:click="addItem">
                          <i class="fa fa-plus" ></i>Add
                         </button>
                      </div>
                     
                    </div>
                    <div class="col-md-9">
                        <draggable class="list-group" element="ul" v-model="list" :options="dragOptions" :move="onMove" @start="isDragging=true" @end="isDragging=false">
                          <transition-group type="transition" :name="'flip-list'">
                            <li class="list-group-item" v-for="(element, index) in list" :key="element.order">
                              <span class="text-primary">@{{ index + 1 }} . </span>
                              <i class="fa fa-map-marker"></i>
                              <input type="hidden" name="trip_description[]" v-model="element.id"  >
                              <i :class="element.fixed? 'fa fa-anchor' : 'glyphicon glyphicon-pushpin'" @click=" element.fixed=! element.fixed" aria-hidden="true"></i>
                              @{{element.label}}
                               <select name="trip_fare[]"  style="width: 120px;
    margin-left: 435px;
    margin-top: -22px;" class="form-control">

                          <template v-if="element.bus_fare == '100'">
                          <option value="100" selected="selected"> ၁၀၀ က်ပ္</option>
                          </template>
                          <template v-else>
                          <option value="100"> ၁၀၀ က်ပ္</option>
                          </template>
                          <template v-if="element.bus_fare == '200'">
                          <option value="200" selected="selected">၂၀၀ က်ပ္</option>
                          </template>
                          <template v-else>
                          <option value="200" >၂၀၀ က်ပ္</option>
                          </template>
                          <template v-if="element.bus_fare == '300'">
                          <option value="300" selected="selected">၃၀၀ က်ပ္</option>
                          </template>
                          <template v-else>
                          <option value="300">၃၀၀ က်ပ္</option>
                          </template>
                          <template v-if="element.bus_fare == '400'">
                          <option value="400" selected="selected">၄၀၀ က်ပ္</option>
                          </template>
                          <template v-else>
                          <option value="400">၄၀၀ က်ပ္</option>
                          </template>
                          <template v-if="element.bus_fare == '500'">
                          <option value="500" selected="selected">၅၀၀ က်ပ္</option>
                          </template>
                          <template v-else>
                          <option value="500">၅၀၀ က်ပ္</option>
                          </template>
                        </select>
                          <select name="trip_group[]"  style="width: 100px;
    margin-left: 560px;
    margin-top: -34px;" class="form-control">

                          <template v-if="element.group == 'a'">
                          <option value="a" selected="selected"> A</option>
                          </template>
                          <template v-else>
                          <option value="a"> A</option>
                          </template>
                          <template v-if="element.group == 'b'">
                          <option value="b" selected="selected">B</option>
                          </template>
                          <template v-else>
                          <option value="b" >B</option>
                          </template>
                          <template v-if="element.group == 'c'">
                          <option value="c" selected="selected">C</option>
                          </template>
                          <template v-else>
                          <option value="c">C</option>
                          </template>
                          <template v-if="element.group == 'd'">
                          <option value="d" selected="selected">D</option>
                          </template>
                          <template v-else>
                          <option value="d">D</option>
                          </template>
                          <template v-if="element.group == 'e'">
                          <option value="e" selected="selected">E</option>
                          </template>
                          <template v-else>
                          <option value="e">E</option>
                          </template>
                        </select>
                              <a style="margin-top: -25px;" class="text-danger pull-right" v-on:click="removeItem(index)"><i class="fa fa-trash-o"></i></a>
                            </li>
                          </transition-group>
                        </draggable>
                    </div>
                  
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="ln_solid"></div>
                        <div class="form-group ">
                        <form action="{{ route('trip.store', $trip->trip_id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                    <a href="{{route('trip.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                </form>
                        </div>
                    </div>
                </form>
               
            </div>
            </div>
            </div>
        </div>
    </div>
            
                
@endsection


@section('busstopscript')
<script src="https://unpkg.com/vue@2.4.2"></script>
<!-- CDNJS :: Sortable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Sortable/1.6.0/Sortable.min.js"></script>

<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.14.1/vuedraggable.min.js"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
      <script src="{{ asset('js/select2.min.js') }}"></script>
      
    <script>
    $('#busstop_township').select2({
      width: 'resolve'
     });

      $('#busstop_street').select2({
         width: 'resolve'
     });

      $.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
Vue.component('v-select', VueSelect.VueSelect);

    const busstops = {!! $busstops !!};
    const lists = {!! $list !!}
    const fares = {!! $fare !!}
    const fares_trip = {!! $fareTrip !!}
var app = new Vue({
    el : '#app',
   data () {
      return {
        message:'message',
        busstops: busstops,
        fares:fares,
        
        list:lists,
        item:'',
        farePrice:'',
        Tripfare:fares_trip,
        editable:true,
        isDragging: false,
        delayedDragging:false
      }
    },
     methods:{
      orderList () {
        this.list = this.list.sort((one,two) =>{return one.order-two.order; })
      },
      onMove ({relatedContext, draggedContext}) {
        const relatedElement = relatedContext.element;
        const draggedElement = draggedContext.element;
        return (!relatedElement || !relatedElement.fixed) && !draggedElement.fixed
      },
      addItem: function(event) {
        event.preventDefault();
        var newItem = this.getNewItem();
        if(! newItem.id) {
          alert('Please Select Bus Stop');
          return false;
        }
        var isExist = this.list.filter(function(item){
            if(item.id == newItem.id) return item.id;
        });
        if(! isExist.length){
          this.list.push(newItem);
          this.item = '';
        }else{
          alert('This Bus Stop already exist');
          this.item = '';
        }
      },
      addFare: function(event) {
        event.preventDefault();
        var newItem = this.getNewFare();
        if(! newItem.fare_price) {
          alert('Please Select Price');
          return false;
        }
        
       
          this.Tripfare.push(newItem);
          this.item = '';
        
      },
      getNewItem: function(){
        return {id: this.item.value , name: this.item.label,code: this.item.code, lat: this.item.lat , long: this.item.long,order: true, listfixed: false, label: this.item.label};
      },
       getNewFare: function(){
        return {fare_price: this.farePrice};
      },
      removeItem : function (index) {
        //this.list.splice(this.list.indexOf(item), 1)
        this.list.splice(index, 1);
      },
      removeFare : function (index) {
        //this.list.splice(this.list.indexOf(item), 1)
        this.Tripfare.splice(index,1);
      },
      
    },
    computed: {
      dragOptions () {
        return  {
          animation: 0,
          group: 'description',
          disabled: !this.editable,
          ghostClass: 'ghost'
        };
      },
      listString(){
        return JSON.stringify(this.list, null, 2);
      }
    }
});



                            </script>   
                            <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
 </script>

@stop