@extends('layouts.admin')

@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@stop

@section('content')
 
    <!-- Main content -->
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        <div id="app">
        <div class="x_panel">
            <div class="x_title">
                  <h2>Create Trip </h2>
                
                            
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
              
              <form id="demo-form2" method="post" action="{{route('trip.store')}}" class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                    <div class="row">
                    
                      <div class="form-group  col-md-6 col-sm-6 col-xs-12">
                          <label for="route_name">Route Name</label>
                          
                          <input type="text" class='form-control' name="route_name" value="{{ old('route_name') }}" required="required" placeholder="Route Name">
                         
                      </div>
                       <div class="form-group  col-md-6 col-sm-6 col-xs-12">
                          <label for="route_name">Service Number</label>
                          
                          <select name="trip_bus"  class="form-control">
                            <option value="0">Choose Service Number</option>
                            @foreach($busregister as $b)
                              <option value="{{$b->bus_id}}">{{$b->bus_name}} </option>
                            @endforeach
                          </select>
                          
                      </div>
                       
                    </div>
                    <div class="row">
                      <div class="form-group col-md-9 col-sm-9 col-xs-12">
                        <label for="bus_stops">Forwards Bus Stops</label>
                         <v-select :options="busstops" v-model="item" placeholder="Select Bus Stop"></v-select>
                      </div>
                      <div class="col-md-3">
                      <label for="bus_stops"></label>
                         <button class="btn btn-primary btn-sm" v-on:click="addItem">
                          <i class="fa fa-plus" ></i>Add
                         </button>
                      </div>
                    
                    </div>
                    <div class="col-md-9">
                        <draggable class="list-group" element="ul" v-model="list" :options="dragOptions" :move="onMove" @start="isDragging=true" @end="isDragging=false">
                          <transition-group type="transition" :name="'flip-list'">
                            <li class="list-group-item" v-for="(element, index) in list" :key="element.order">
                            
                              <span class="text-primary">@{{ index + 1 }} . </span>
                              <i class="fa fa-map-marker"></i>
                              <input type="hidden" name="trip_description[]" v-model="element.id"  >
                              <i :class="element.fixed? 'fa fa-anchor' : 'glyphicon glyphicon-pushpin'" @click=" element.fixed=! element.fixed" aria-hidden="true"></i>
                              @{{element.label}}
                            
                              
                               <select name="trip_fare[]" style="
    width: 120px;
    margin-left: 435px;
    margin-top: -22px;
" class="form-control">
                          
                          <option value="100">၁၀၀ က်ပ္</option>
                          <option value="200">၂၀၀ က်ပ္</option>
                          <option value="300">၃၀၀ က်ပ္</option>
                          <option value="400">၄၀၀ က်ပ္</option>
                          <option value="500">၅၀၀ က်ပ္</option>
                          
                        </select>
                          <select name="trip_group[]" style="
    width: 100px;
    margin-left: 560px;
    margin-top: -34px;
" class="form-control">
                          
                          <option value="a">A</option>
                          <option value="b">B</option>
                          <option value="c">C</option>
                          <option value="d">D</option>
                          <option value="e">E</option>
                          
                        </select>
                              <a style="margin-top: -25px;" class="text-danger pull-right" v-on:click="removeItem(index)"><i class="fa fa-trash-o"></i></a>
                             
                            </li>
                          </transition-group>
                        </draggable>

                    </div>
                    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="ln_solid"></div>
                        <div class="form-group ">
                        <a href="{{route('trip.index')}}" class="btn btn-success"> <i class="fa fa-close"></i> Cancel</a>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </form>
               
            </div>
            </div>
            </div>
        </div>
    </div>
            
                
@endsection


@section('busstopscript')

<script src="https://unpkg.com/vue@2.4.2"></script>
<!-- CDNJS :: Sortable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Sortable/1.6.0/Sortable.min.js"></script>

<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.14.1/vuedraggable.min.js"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>      
      
    <script>
     $('#trip_bus').select2({
     
     });
   </script>
   <script type="text/javascript">
     
Vue.component('v-select', VueSelect.VueSelect);

    const busstops = {!! $busstops !!};
    
    const fares = {!! $fare !!}
var app = new Vue({
    el : '#app',
   data () {
      return {
        message:'message',
        busstops: busstops,
        fares:fares,
        fare:'',
        list:[],
        item:'',
        farePrice:'',
        fare:[],
        editable:true,
        isDragging: false,
        delayedDragging:false
      }
    },
     methods:{
      orderList () {
        this.list = this.list.sort((one,two) =>{return one.order-two.order; })
      },
      onMove ({relatedContext, draggedContext}) {
        const relatedElement = relatedContext.element;
        const draggedElement = draggedContext.element;
        return (!relatedElement || !relatedElement.fixed) && !draggedElement.fixed
      },
      addItem: function(event) {
        event.preventDefault();
        var newItem = this.getNewItem();
        if(! newItem.id) {
          alert('Please Select Bus Stop');
          return false;
        }
        var isExist = this.list.filter(function(item){
            if(item.id == newItem.id) return item.id;
        });
        if(! isExist.length){
          this.list.push(newItem);
          this.item = '';
        }else{
          alert('This Bus Stop already exist');
          this.item = '';
        }
      },
      addFare: function(event) {
        event.preventDefault();
        var newItem = this.getNewFare();
        if(! newItem.fare_price) {
          alert('Please Select Price');
          return false;
        }
        
       
          this.fare.push(newItem);
          this.item = '';
        
      },
      getNewItem: function(){
        return {id: this.item.value , name: this.item.label,code: this.item.code, lat: this.item.lat , long: this.item.long,order: true, listfixed: false, label: this.item.label};
      },
       getNewFare: function(){
        return {fare_price: this.farePrice};
      },
      removeItem : function (index) {
        //this.list.splice(this.list.indexOf(item), 1)
        this.list.splice(index, 1);
      },
      
    },
    computed: {
      dragOptions () {
        return  {
          animation: 0,
          group: 'description',
          disabled: !this.editable,
          ghostClass: 'ghost'
        };
      },
      listString(){
        return JSON.stringify(this.list, null, 2);
      }
    }
});



                            </script>   
                            <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
 </script>

@stop