@extends('layouts.admin')

@section('content')
 
    <!-- Main content -->
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Create Township </h2>
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
              <form id="demo-form2" method="post" action="{{route('townships.store')}}" class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="township_name">Township Name</label>
                        <input type="text" class='form-control' name="township_name" value="{{ old('township_name') }}" required="required" placeholder="Township Name">
                    </div>
                    
                        <div class="ln_solid"></div>
                      <a href="{{route('townships.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                </form>
            </div>
            </div>
        </div>
    </div>
            
                
           
    
@endsection
