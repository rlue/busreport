@extends('layouts.admin')

@section('content')


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Township Edit </h2>
                
                      
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                <form method="post" action="{{route('townships.update',$township->id)}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Township Name</label>
                        <input type="text" class='form-control' name="township_name" value="{{$township->township_name}}" placeholder="Township " required="required">
                    </div>
                   
                        <div class="ln_solid"></div>
                    <form action="{{ route('townships.store', $township->id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                    <a href="{{route('townships.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                </form>
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection
            
           

