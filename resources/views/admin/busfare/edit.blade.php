@extends('layouts.admin')

@section('content')


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>Edit Bus </h2>
                
                        
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">

               @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                <form method="post" action="{{route('bus.update',$bus->bus_id)}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="bus_name">Bus Name</label>
                        <input type="text" class='form-control' name="bus_name" value="{{$bus->bus_name}}" placeholder="Bus Name " required="required">
                    </div>
                   
                        <div class="ln_solid"></div>
                    <form action="{{ route('bus.store', $bus->bus_id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                     <a href="{{route('bus.index')}}" class="btn btn-success"><i class="fa fa-close"></i> Cancel</a> 
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                </form>
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection
            
           

