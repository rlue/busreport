@extends('layouts.admin')
@section('busstopstyle')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@stop
@section('content')

            <div class="page-title">
              <div class="title_left">

                <h3>Detail Trip Export<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
               
              </div>
            </div>

            <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <div class="x_panel">
            <div class="x_title">
                  <h2>By Trip </h2>
                
                
                          
                  <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <div class="row">
            <form action="{{route('byTripExport')}}" method="POST">
            {{ csrf_field() }}
                <div class="form-group col-md-3">
                <label class="">Company</label>
                   <select name="getCompany" id="getCompany" class="form-control">
                       <option value>All</option>
                       @foreach($company as $c)
                        <option value="{{$c->company_id}}">{{$c->company_name}}</option>
                       @endforeach
                   </select>     
                </div>
                <div class="form-group col-md-3">
                <label class="">Service No</label>
                   <select name="getBus" id="getBus" class="form-control">
                      
                      @foreach($bus as $b)
                      <option value="{{$b->bus_id}}">{{$b->bus_name}}</option>
                      @endforeach
                   </select>     
                </div>
                <div class="form-group col-md-2">
                    <label>Date</label>
                    <input type="text" class="form-control" name="startDate" id="startDate" >
                </div>
               
                <div class="form-group col-md-2">
                <label>File Type</label>
                      <select name="file_type" class="form-control col-md-2">
                        <option value="xls">XLS</option>
                         <option value="xlsx">XLSX</option>
                          <option value="csv">CSV</option>
                    </select>
                </div>
                <div class="form-group col-md-12">
                  @can('report')
                    <button type="submit" class="btn btn-success" id="submit"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export</button>
                    @endcan
                </div>
            </form>
            </div>
               
            </div>
            </div>
        </div>
    </div>
 
@endsection

@section('busstopscript')

      <script src="{{ asset('js/select2.min.js') }}"></script>
      <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
        
        $(document).ready(function(){
           $('#getCompany').select2();
             
            $('#getBus').select2();
            $('#submit').click(function(){
                var start = $('#startDate').val();
                
                if(start){
                       return true;
                }else{
                    alert('Please fill StartDate ');
                    return false;
                }
                
            });
        });
    </script>
     <!-- <script type="text/javascript">
         $(document).ready(function(){
            $('#getCompany').select2();
             
            $('#getBus').select2();
            
            $('#getCompany').on('change',function(){
                var companyid = $(this).val();
                $('#getBus')
    .empty()
    .append('<option selected="selected" value="0">All</option>')
;
                $.ajax({
                    type: "POST",
                    url: '<?php echo route("getBusName"); ?>',
                    data: {comid:companyid},
                    success: function(data) {
                       $.each(data, function(key, value) {   
                            $('#getBus').append($('<option>', { 
                          value: value.bus_id,
                          text : value.bus_name 
                      })); 
                      });
                    }
                });
            });
         });
     </script> -->
     <script type="text/javascript">
  $(function() {
   $('#startDate').datetimepicker({
     format: 'YYYY-MM-DD'
   });
   $('#endDate').datetimepicker({
     format: 'YYYY-MM-DD'
   });
  });
</script>
   <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
 </script>

@stop

