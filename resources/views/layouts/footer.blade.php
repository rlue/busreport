 <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- jQuery -->
    
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap-progressbar.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/nprogress.js') }}"></script>

    <!-- NProgress -->
   <script src="{{ asset('js/summernote.min.js') }}"></script>
  
    <!-- Custom Theme Scripts -->
 <script type="text/javascript">
     
     $(document).ready(function(){

        $('.remove_item').click(function(){
         
            var con =  confirm('Are you sure to delete?');
           
            if(con){
             
              return true;

            }else{
              
              return false;
            }
        });
     });
   </script>
   
  @yield('busstopscript')
  <script src="{{ asset('js/custom.min.js') }}"></script>
   

