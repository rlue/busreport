<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>YGN Bus Station | Bus Report System</title>

    <!-- Bootstrap -->
   <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" >
    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="{{ asset('css/custom.min.css') }}">
    <!-- data table -->
     <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
  </head>

  <body class="login">
    @yield('content')
  </body>
</html>
