@include('layouts.header')
  <body class="nav-md">
  <div id="app">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <span>YGN Bus Station</span></a>
            </div>

            <div class="clearfix"></div>

           
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                @can('report')
                  <li><a><i class="fa fa-file-excel-o"></i> All Report Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('busfare.index')}}">Report By Bus</a></li>
                      <li><a href="{{route('bybus.index')}}">Report By Route</a></li>
                      <li><a href="{{route('byTrip')}}">Report By Trip</a></li>
                      <li><a href="{{route('byGas')}}">Gas Report</a></li>
                    </ul>
                  </li>
                  @endcan
                  @if(Auth::user()->is_super)
                  <li><a><i class="fa fa-user"></i> User Manage <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <!-- <li><a href="{{route('users.create')}}">Add New User</a></li> -->
                      <li><a href="{{route('users.index')}}">User List</a></li>
                       <li><a href="{{route('roles.index')}}">Role List</a></li>
                      <li><a href="{{route('permissions.index')}}">Permission List</a></li>
                    </ul>
                  </li>
                   @endif 
                 <li><a><i class="fa fa-edit"></i> Bus Stop <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('busstop.index')}}">Bus Stop</a></li>
                      <li><a href="{{route('townships.index')}}">Township</a></li>
                      <li><a href="{{route('street.index')}}">Street</a></li>
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-car"></i> Bus INFO <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('driver.index')}}">Bus Captain</a></li>
                    <li><a href="{{route('device.index')}}">Device</a></li>
                    <li><a href="{{route('bus.index')}}">Bus Service Number</a></li>
                    <li><a href="{{route('busgas')}}">Bus Gas</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-tasks"></i> Bus Operator Company <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('company.index')}}">Company</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-group"></i> Inspector Information <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('inspector.index')}}">Inspector Manage</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cab"></i> Bus Registration Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('busregister.index')}}">Bus Register</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-map-marker"></i> Trip Builder <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('trip.index')}}">Route</a></li>
                    </ul>
                  </li>

                </ul>
              </div>
              
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a> 
              </div>
                @if (Auth::guest())
                  <h2>Login</h2>
              @else
                 <ul class="nav navbar-nav navbar-right">
                <li class="">

                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <!-- <img src="images/img.jpg" alt=""> -->{{ Auth::user()->name }}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                   
                    <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
                  </ul>
                </li>

                
              </ul>
              @endif
             
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <div class="right_col" role="main">
          <!-- top tiles -->
         
           @yield('content')
         
        </div>

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
  </div>
   @include('layouts.footer')
   
   </body>
</html>
