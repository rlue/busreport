@extends('layouts.login')

@section('content')

                   
               <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <img src="{{ asset('image/logo.png') }}">
            <form  method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <h1>Login Form</h1>
              <div>
               @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                               
              </div>
              <div>
                 <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
              </div>
              <div>
                 <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                
                            
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                

                <div class="clearfix"></div>
                <br />

                <div>
                  
                  <p>©2017 All Rights Reserved</p>
                </div>
              </div>
                       
                    </form>
          </section>
        </div>

        
      </div>
    </div>
@endsection
