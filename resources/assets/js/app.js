Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));



const app = new Vue({
    el: '#app',
     methods : {

       

        createItem: function(){
		  var input = this.newItem;
      
		  this.$http.post('/townships/store',input).then((response) => {
		    this.changePage(this.pagination.current_page);
			this.newItem = {'title':'','description':''};
			$("#create-item").modal('hide');
			toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000});
		  }, (response) => {
			this.formErrors = response.data;
	    });
	}

     
  }

});
