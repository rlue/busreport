<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






$this->get('/login', 'Auth\LoginController@showLoginForm')->name('login');
 $this->get('register', 'HomeController@index')->name('login');
        $this->post('/login', 'Auth\LoginController@login');
Route::group(['middleware' => 'auth'], function () {

	Route::get('/', 'Admin\TripController@index');
	//role route
    Route::get("roles", 'Admin\RoleController@index')->name("roles.index");
    Route::get("roles/create", 'Admin\RoleController@create')->name("roles.create");
    Route::get("roles/{id}/edit", 'Admin\RoleController@edit')->name("roles.edit");
    Route::get("roles/delete/{id}", 'Admin\RoleController@destroy')->name("roles.delete");
    Route::patch('roles/update/{id}',"Admin\RoleController@update")->name('roles.update');
    Route::post('roles/store', 'Admin\RoleController@store')->name("roles.store");

    //Permission route
    Route::get("permissions", 'Admin\PermissionController@index')->name("permissions.index");
    Route::get("permissions/create", 'Admin\PermissionController@create')->name("permissions.create");
    Route::get("permissions/{id}/edit", 'Admin\PermissionController@edit')->name("permissions.edit");
    Route::get("permissions/delete/{id}", 'Admin\PermissionController@destroy')->name("permissions.delete");
    Route::patch('permissions/update/{id}',"Admin\PermissionController@update")->name('permissions.update');
    Route::post('permissions/store', 'Admin\PermissionController@store')->name("permissions.store");


    Route::get('users','Admin\UserController@index')->name("users.index");
    Route::get("users/create", 'Admin\UserController@create')->name('users.create');
    Route::post("users/store",'Admin\UserController@store')->name("users.store");
    Route::get('users/{id}/edit',"Admin\UserController@edit")->name('users.edit');
    Route::patch('users/update/{id}',"Admin\UserController@update")->name('users.update');
    Route::get('users/delete/{id}',"Admin\UserController@destroy")->name('users.delete');

    Route::get('townships','Admin\TownshipController@index')->name("townships.index");
    Route::get("townships/create", 'Admin\TownshipController@create')->name('townships.create');
    Route::post("townships/store",'Admin\TownshipController@store')->name("townships.store");
    Route::get('townships/{id}/edit',"Admin\TownshipController@edit")->name('townships.edit');
    Route::patch('townships/update/{id}',"Admin\TownshipController@update")->name('townships.update');
    Route::get('townships/delete/{id}',"Admin\TownshipController@destroy")->name('townships.delete');

    Route::get('street','Admin\StreetController@index')->name("street.index");
    Route::get("street/create", 'Admin\StreetController@create')->name('street.create');
    Route::post("street/store",'Admin\StreetController@store')->name("street.store");
    Route::get('street/{id}/edit',"Admin\StreetController@edit")->name('street.edit');
    Route::patch('street/update/{id}',"Admin\StreetController@update")->name('street.update');
    Route::get('street/delete/{id}',"Admin\StreetController@destroy")->name('street.delete');

    Route::get('busstop','Admin\BusStopController@index')->name("busstop.index");
    Route::get("busstop/create", 'Admin\BusStopController@create')->name('busstop.create');
    Route::post("busstop/store",'Admin\BusStopController@store')->name("busstop.store");
    Route::get('busstop/{id}/edit',"Admin\BusStopController@edit")->name('busstop.edit');
    Route::patch('busstop/update/{id}',"Admin\BusStopController@update")->name('busstop.update');
    Route::get('busstop/delete/{id}',"Admin\BusStopController@destroy")->name('busstop.delete');
   
///for edit table script
   Route::post('busUpdate','Admin\BusStopController@quickUpdate')->name('busUpdate');
   Route::get('street/list','Admin\BusStopController@streetList')->name('street.list');
   Route::get('township/list','Admin\BusStopController@townshipList')->name('township.list');

   //for driver 
   Route::get('driver','Admin\DriverController@index')->name("driver.index");
    Route::get("driver/create", 'Admin\DriverController@create')->name('driver.create');
    Route::post("driver/store",'Admin\DriverController@store')->name("driver.store");
    Route::get('driver/{id}/edit',"Admin\DriverController@edit")->name('driver.edit');
    Route::patch('driver/update/{id}',"Admin\DriverController@update")->name('driver.update');
    Route::get('driver/delete/{id}',"Admin\DriverController@destroy")->name('driver.delete');

     //for Device 
   Route::get('device','Admin\DeviceController@index')->name("device.index");
    Route::get("device/create", 'Admin\DeviceController@create')->name('device.create');
    Route::post("device/store",'Admin\DeviceController@store')->name("device.store");
    Route::get('device/{id}/edit',"Admin\DeviceController@edit")->name('device.edit');
    Route::patch('device/update/{id}',"Admin\DeviceController@update")->name('device.update');
    Route::get('device/delete/{id}',"Admin\DeviceController@destroy")->name('device.delete');

     //for bus 
   Route::get('bus','Admin\BusController@index')->name("bus.index");
    Route::get("bus/create", 'Admin\BusController@create')->name('bus.create');
    Route::post("bus/store",'Admin\BusController@store')->name("bus.store");
    Route::get('bus/{id}/edit',"Admin\BusController@edit")->name('bus.edit');
    Route::patch('bus/update/{id}',"Admin\BusController@update")->name('bus.update');
    Route::get('bus/delete/{id}',"Admin\BusController@destroy")->name('bus.delete');

    // for company
    Route::get('company','Admin\CompanyController@index')->name("company.index");
    Route::get("company/create", 'Admin\CompanyController@create')->name('company.create');
    Route::post("company/store",'Admin\CompanyController@store')->name("company.store");
    Route::get('company/{id}/edit',"Admin\CompanyController@edit")->name('company.edit');
    Route::patch('company/update/{id}',"Admin\CompanyController@update")->name('company.update');
    Route::get('company/delete/{id}',"Admin\CompanyController@destroy")->name('company.delete');

     // for inspector
    Route::get('inspector','Admin\InspectorController@index')->name("inspector.index");
    Route::get("inspector/create", 'Admin\InspectorController@create')->name('inspector.create');
    Route::post("inspector/store",'Admin\InspectorController@store')->name("inspector.store");
    Route::get('inspector/{id}/edit',"Admin\InspectorController@edit")->name('inspector.edit');
    Route::patch('inspector/update/{id}',"Admin\InspectorController@update")->name('inspector.update');
    Route::get('inspector/delete/{id}',"Admin\InspectorController@destroy")->name('inspector.delete');

     // for Bus Register
    Route::get('busregister','Admin\BusRegisterController@index')->name("busregister.index");
    Route::get("busregister/create", 'Admin\BusRegisterController@create')->name('busregister.create');
    Route::post("busregister/store",'Admin\BusRegisterController@store')->name("busregister.store");
    Route::get('busregister/{id}/edit',"Admin\BusRegisterController@edit")->name('busregister.edit');
    Route::patch('busregister/update/{id}',"Admin\BusRegisterController@update")->name('busregister.update');
    Route::get('busregister/delete/{id}',"Admin\BusRegisterController@destroy")->name('busregister.delete');

    // for Trip
    Route::get('trip','Admin\TripController@index')->name("trip.index");
    Route::get("trip/create", 'Admin\TripController@create')->name('trip.create');
    Route::post("trip/store",'Admin\TripController@store')->name("trip.store");
    Route::get('trip/{id}/edit',"Admin\TripController@edit")->name('trip.edit');
    Route::patch('trip/update/{id}',"Admin\TripController@update")->name('trip.update');
    Route::get('trip/delete/{id}',"Admin\TripController@destroy")->name('trip.delete');

     Route::post("trip/map",'Admin\TripController@getMap')->name("trip.map");

    // for Bus Service Registration
    Route::get('busservice','Admin\BusServiceController@index')->name("busservice.index");
    Route::get("busservice/create", 'Admin\BusServiceController@create')->name('busservice.create');
    Route::post("busservice/store",'Admin\BusServiceController@store')->name("busservice.store");
    Route::get('busservice/{id}/edit',"Admin\BusServiceController@edit")->name('busservice.edit');
    Route::patch('busservice/update/{id}',"Admin\BusServiceController@update")->name('busservice.update');
    Route::get('busservice/delete/{id}',"Admin\BusServiceController@destroy")->name('busservice.delete');

    //call by bus form
    Route::get('busfare',"Admin\BusFareController@index")->name('busfare.index');
    //export by bus data
    Route::post('byBus',"Admin\BusFareController@byBusExport")->name('byBus');

    //call by Route form
    Route::get('byroute',"Admin\BusFareController@ByRoute")->name('bybus.index');
    //export by Route data
    Route::post('byRouteExport',"Admin\BusFareController@byRouteExport")->name('byRouteExport');

    //call by Trip form
    Route::get('byTrip',"Admin\BusFareController@ByTrip")->name('byTrip');
    //export by Trip data
    Route::post('byTripExport',"Admin\BusFareController@byTripExport")->name('byTripExport');

    //call by Gas form
    Route::get('byGas',"Admin\BusFareController@ByGas")->name('byGas');
    //export by Gas data
    Route::post('byGasExport',"Admin\BusFareController@byGasExport")->name('byGasExport');

    //call bus gas List
    Route::get('busgas',"Admin\BusController@busGasList")->name('busgas');

    //Get bus name with company
    Route::post('getBusName',"Admin\BusFareController@getBusName")->name('getBusName');
});
Auth::routes();


