<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getbusfare', 'Api\BusApiFareController@index');
Route::post('/busfare', 'Api\BusApiFareController@store');

Route::get('/getbusgas', 'Api\GasApiController@index');
Route::post('/busgas', 'Api\GasApiController@store');


//driver info api
Route::post('/driver', 'Api\DriverApiController@index');

//Inspector info api
Route::post('/inspector', 'Api\InspectorApiController@index');
//get busstop data
Route::get('/busstop', 'Api\BusStopApiController@index');

//insert terminate bus data
Route::post('/terminate', 'Api\BusTerminateApiController@index');

//get bus info with deviceid
Route::post('/device', 'Api\DeviceApiController@index');